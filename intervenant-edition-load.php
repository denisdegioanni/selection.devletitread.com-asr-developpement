<?
require_once(dirname(__FILE__).'/include/config.inc.php');
require_once(dirname(__FILE__).'/include/fonctions.inc.php');

$db = common_mysql_connect(MYSQL_DB);

ident_page_protect(9);//Gestion admin

$ID_intervenant = $_REQUEST['id'];

if($ID_intervenant == '0')
{
	?>
	$('#intervenantEditForm input[name="ID_intervenant"]').val("0");	
	$('#intervenantEditForm input[type=text],#intervenantEditForm input[type=date],#intervenantEditForm input[type=number]').val('');	
	$('#intervenantEditFormLabel').html('Nouvelle ressource');
	<?
}
elseif($intervenant_datas = intervenants_get_datas($db,$ID_intervenant))//Verification droit
{
   	?>
   	if($('#intervenantEditForm').length> 0)
   	{
   		$('#intervenantEditForm input[name="ID_intervenant"]').val("<? print $ID_intervenant;?>");
   		$('#intervenantEditForm select[name="ID_admrole"]').val('<? print addslashes($intervenant_datas['ID_admrole']);?>');
   		$('#intervenantEditForm input[name="nom"]').val('<? print addslashes($intervenant_datas['intervenant_nom']);?>');
   		$('#intervenantEditForm input[name="prenom"]').val('<? print addslashes($intervenant_datas['intervenant_prenom']);?>');
   		$('#intervenantEditForm input[name="upd_login"]').val('<? print addslashes($intervenant_datas['intervenant_login']);?>');
   		$('#intervenantEditForm input[name="upd_pwd"]').val('<? print addslashes($intervenant_datas['intervenant_pwd']);?>');
   		$('#intervenantEditForm input[name="email"]').val('<? print addslashes($intervenant_datas['intervenant_email']);?>');
   		
   		$('#intervenantEditFormLabel').html('<? print addslashes($intervenant_datas['intervenant_nom'].' '.$intervenant_datas['intervenant_prenom']);?>');
   	}
   	<?		
}
else
{
	?>
	alert("Fiche inexistante (ne peut être modifiée).");
	<?
	exit();		
}

//Tout est ok, on affiche le contenu
?>
$('#intervenantEditForm').fadeIn();