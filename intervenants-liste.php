<?
require("include/config.inc.php");
require("include/fonctions.inc.php");
$db = common_mysql_connect(MYSQL_DB);
require("include/header.inc.php");

ident_page_protect(array(9));//Gestion intervenants

?>
<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->

        <ul class="breadcrumb">
            <li><a href="#">Accueil</a></li>

            <li><a href="#">Administration</a></li>

            <li class="active">Liste intervenants</li>
        </ul><!--breadcrumbs end -->
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
		<?
		//Recherche des programmes
		$result_liste 	= intervenants_search( $db, 0, 9999, '', '' );
		$nb_total	 	= $result_liste['nb_results_total'];
		$nb_liste	 	= $result_liste['nb_results_liste'];
		$datas		 	= $result_liste['results'];
		$i = 0;
		
		if($nb_liste > 0)
		{
		    ?>
            <section class="panel">
                <header class="panel-heading">
                    Accès backoffice
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                     </span>
                </header>
                <div class="panel-body">
	                <div class="adv-table">
	              
						<div class="clearfix">
		                    <div class="btn-group">
		                        <button data-toggle="modal" data-target="#intervenantEditModal" class="btn btn-primary" onClick="intervenantEdit('0');">
		                            Ajouter <i class="fa fa-plus"></i>
		                        </button>
		                    </div>
		                </div>                
	
						<table  class="display table table-bordered table-striped dynamic-table">
						<thead>
						<tr>
						    <th>Nom</th>
						    <th>Type</th>
						    <th>Dernier accès</th>
						    <th>Actions</th>
						</tr>
						</thead>
						<tbody>
						    <?  
						    foreach($datas as $id => $null)
						    {
						    	if(is_array($null))
						    	{
						        	$ID_intervenant 	= $datas[$id]['ID_intervenant'];
						        	$intervenants_datas	= intervenants_get_datas($db,$ID_intervenant);	
						        	
						        	$admrole_datas		= cfg_admin_roles_get_datas($db,$intervenants_datas['ID_admrole']);
						        	?>
						    		<tr class="gradeX" id="intervenant_liste_<? print $ID_intervenant?>">

						    		    <td>
						    				<a href="#edit" data-toggle="modal" data-target="#intervenantEditModal" onClick="intervenantEdit('<? print $ID_intervenant;?>');">								    
						    					<?
						    					print str_truncate(str_txt_format($intervenants_datas['intervenant_nom']).' '.str_txt_format($intervenants_datas['intervenant_prenom']), 200);
						    					?>									
						    	    		</a>
						    		    </td>
						    		    <td>
						    		    	<? print $admrole_datas['admrole_name'];?>
						    		    </td>							    
						    		    <td>
						    				<a href="#edit" data-toggle="modal" data-target="#intervenantEditModal" onClick="intervenantEdit('<? print $ID_intervenant;?>');">								    
						    					<? 
						    					if($intervenants_datas['intervenant_lastaccess'] != '0000-00-00 00:00:00')
						    					{
							    					print date('d/m/Y à H:i', strtotime($intervenants_datas['intervenant_lastaccess']));							    					
						    					}
						    					else
						    					{
							    					print '---------------------------';
						    					}
						    					?>
						    				</a>								    
						    		    </td>
						    		    <!-- 'DATEATT','DATEOK','DATEERR','ELEMENTATT','ELEMENTSOK','BATREALISE','BATMODIF','BATOK','TIRAGE','APOSER','FINI' -->
						    		    <td class="center">
						    				<a href="#edit" data-toggle="modal" data-target="#intervenantEditModal"  onClick="intervenantEdit('<? print $ID_intervenant;?>');">								    
						    					editer
						    				</a>	
						    				|									
						    				<a href="#suppr" data-toggle="modal" data-target="#confirmDeleteModal"  onClick="intervenantDeleteConfirm('<? print $ID_intervenant;?>');">								    
						    					supprimer
						    				</a>	
						    											    
						    		    </td>
						    		</tr>
						        	<?
						        }
						    }
						    ?>
						</tbody>
						<tfoot>
						<tr>
						    <th>Nom</th>
						    <th>Type</th>
						    <th>Dernier accès</th>
						    <th>Actions</th>
						</tr>
						</tfoot>
						</table>
	                </div>
                </div>
            </section>
		    <?
		}
		else
		{
		    ?>
		    <p align="center">
		    	Aucun accès administration
				<div class="clearfix">
				    <div class="btn-group">
				        <button data-toggle="modal" data-target="#intervenantEditModal" class="btn btn-primary" onClick="intervenantEdit('0');">
				            Ajouter <i class="fa fa-plus"></i>
				        </button>
				    </div>
				</div>  		    
		    </p>
		    <?
		}
		?>
	</div>
</div>		

<?
require("include/footer.inc.php");
?>