<?
require("include/config.inc.php");
require("include/fonctions.inc.php");
$db = common_mysql_connect(MYSQL_DB);
require("include/header.inc.php");

ident_page_protect(1);//Gestion clients

$ID_client = $_REQUEST['id'];


if($client_datas = th_clients_get_datas($db,$ID_client))
{

	if($_REQUEST['logodel'] == 1 && $client_datas['client_logo'] != '')
	{
		@unlink(CFG_PATH_UPLOADS.$client_datas['client_logo']);
		th_clients_update_field($db, $ID_client, 'client_logo', '');	
		common_header_redirect('clients-detail.php?id='.$ID_client);
	}
	?>
	<div class="row">
	    <div class="col-md-12">
	        <!--breadcrumbs start -->
	
	        <ul class="breadcrumb">
	            <li><a href="#">Accueil</a></li>
	
	            <li><a href="#">Administration</a></li>
	
	            <li ><a href="clients-liste.php">Liste clients</a></li>
	            
	            <li class="active"><? print $client_datas['client_rs'];?></li>
	        </ul><!--breadcrumbs end -->
	    </div>
	</div>

	<div class="row">
		
		<div class="col-md-12">
            <section class="panel">
                <div class="panel-body profile-information">
                   <div class="col-md-3">
                       <div class="profile-pic text-center">
                           <!--<img src="img/client_type_<? print $client_datas['client_type'];?>.jpg" alt="">-->
                           <?
	                       if($client_datas['client_logo'] != '')
	                       {
		                       ?>
		                       <img src="<? print CFG_PATH_UPLOADS_RELATIVE.$client_datas['client_logo'];?>" class="img-responsive" border="0">
		                       <?
	                       }
	                       else
	                       {
		                       $qr_adresse_map = urlencode($client_datas['client_adresse'].' '.$client_datas['client_adresse_suite'].','.$client_datas['client_cpost'].' '.$client_datas['client_ville'].','.$MYSQL_ENUM_DATA_CLIENT_PAYS[$client_datas['client_pays']]);    
		                       ?>
	                           <a href="http://maps.google.com/?q=<? print $qr_adresse_map;?>" target="_blank">
		                           <img src="https://maps.googleapis.com/maps/api/staticmap?center=<? print $qr_adresse_map;?>&zoom=16&size=180x180" class="img-responsive" border="0">
	                           </a>
		                       <?
	                       }
	                       ?>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="profile-desk">
                           <h1><? print $client_datas['client_rs'];?></h1>
                           <span class="text-muted"><? print $MYSQL_ENUM_DATA_CLIENT_TYPE[$client_datas['client_type']];?></span>
                           <p>
                               <? 
	                            print $client_datas['client_adresse'];
								if($client_datas['client_adresse_suite'] != '')
								{
		                           print '<br/>'.$client_datas['client_adresse_suite'];
	                           	}    
							   	print '<br/>'.$client_datas['client_cpost'].' '.$client_datas['client_ville'];
							   	print '<br/>'.$MYSQL_ENUM_DATA_CLIENT_PAYS[$client_datas['client_pays']];
	                           ?>
                           </p>
                           <a href="#edit" data-toggle="modal" data-target="#clientEditModal"  onClick="clientEdit('<? print $ID_client;?>');" class="btn btn-block btn-primary">Editer les informations <i class="fa fa-edit"></i></a>                          
						   
						   <?
														?>
                       </div>
                   </div>
                   <div class="col-md-3">
                       <div class="profile-statistics">
                           <h1><? print date(CFG_DATE_SHORT_FORMAT, strtotime($client_datas['client_depuis']));?></h1>
                           <p>Client depuis</p>
                           <h1><? print th_residences_nb_for_client($db, $ID_client);?></h1>
                           <p># residences</p>
                       </div>
                   </div>
                </div>
            </section>
        </div>		

	</div>
	
	<div class="row">
	    <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Liste des contacts
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                     </span>
                </header>
                <div class="panel-body">		    
					<?
					//Recherche des contacts
					$result_liste 	= th_clients_contacts_search( $db, 0, 9999, '', ',contact_nom,contact_prenom,contact_fonction,contact_email', $ID_client );
					$nb_total	 	= $result_liste['nb_results_total'];
					$nb_liste	 	= $result_liste['nb_results_liste'];
					$datas		 	= $result_liste['results'];
					$i = 0;
					
					if($nb_liste > 0)
					{
					    ?>
		                <div class="adv-table">
		              
							<div class="clearfix">
			                    <div class="btn-group">
			                        <button data-toggle="modal" data-target="" class="btn btn-primary" onClick="">
			                            Ajouter un contact <i class="fa fa-plus"></i>
			                        </button>
			                    </div>
			                </div>                
		
							<div>Ajouter table </div>
							
		                </div>

			    <?
				}
				else
				{
				    ?>
				    <p align="center">
				    	Aucun contact actuellement pour ce client
						<div class="clearfix">
						    <div class="btn-group">
						        <button data-toggle="modal" data-target="#" class="btn btn-primary" onClick="">
						            Ajouter un contact <i class="fa fa-plus"></i>
						        </button>
						    </div>
						</div>  		    
				    </p>
				    <?
				}
				?>
               </div>
            </section>			
		</div>
	</div>		
	
	<div class="row">
	    <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Liste des résidences
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                     </span>
                </header>
                <div class="panel-body">		    
					<?
					//Recherche des residences
					$result_liste 	= th_residences_search( $db, 0, 9999, '', ',residence_nom,residence_adresse,residence_cpost,residence_ville,residence_batiment,residence_entree', $ID_client );
					$nb_total	 	= $result_liste['nb_results_total'];
					$nb_liste	 	= $result_liste['nb_results_liste'];
					$datas		 	= $result_liste['results'];
					$i = 0;
					
					if($nb_liste > 0)
					{
					    ?>
		                <div class="adv-table">
		              
							<div class="clearfix">
			                    <div class="btn-group">
			                        <button data-toggle="modal" data-target="#residenceEditModal" class="btn btn-primary" onClick="residenceEdit('0', '<? print $ID_client;?>');">
			                            Ajouter une résidence <i class="fa fa-plus"></i>
			                        </button>
			                    </div>
			                </div>                
		
							<table  class="display table table-bordered table-striped dynamic-table">
							<thead>
							<tr>
							    <th width="200">Nom</th>
							    <th>Adresse</th>
							    <th width="250">Actions</th>
							</tr>
							</thead>
							<tbody>
							    <?  
							    foreach($datas as $id => $null)
							    {
							    	if(is_array($null))
							    	{
							        	$ID_residence 	= $datas[$id]['ID_residence'];
							        	$nom 			= $datas[$id]['residence_nom'];
							        	$adresse 		= $datas[$id]['residence_adresse'];
							        	$cpost 			= $datas[$id]['residence_cpost'];
							        	$ville 			= $datas[$id]['residence_ville'];

							        	?>
							    		<tr class="gradeX" id="residence_liste_<? print $ID_residence?>">
	
							    		    <td>
							    				<a href="#edit" data-toggle="modal" data-target="#residenceEditModal" onClick="residenceEdit('<? print $ID_residence;?>');">								    
							    					<?
							    					print str_truncate(str_txt_format($nom), 30);
							    					?>									
							    	    		</a>
							    		    </td>
							    		    <td>
							    					<?
							    					print str_truncate($adresse.', '.$cpost.' '.$ville, 40);
							    					?>									
							    		    </td>						    		    							    		    						    		    
							    		    <td class="center">
							    				<a href="#edit" data-toggle="modal" data-target="#residenceEditModal"  onClick="residenceEdit('<? print $ID_residence;?>');">								    
							    					modifier
							    				</a>	
							    				|									
							    				<a href="#suppr" data-toggle="modal" data-target="#confirmDeleteModal"  onClick="residenceDeleteConfirm('<? print $ID_residence;?>');">								    
							    					supprimer
							    				</a>	
	
							    											    
							    		    </td>
							    		</tr>
							        	<?
							        }
							    }
							    ?>
							</tbody>
							<tfoot>
							<tr>
							    <th>Nom</th>
							    <th>Adresse</th>
							    <th>Actions</th>
							</tr>
							</tfoot>
							</table>
		                </div>

			    <?
				}
				else
				{
				    ?>
				    <p align="center">
				    	Aucune résidence actuellement pour ce client
						<div class="clearfix">
						    <div class="btn-group">
						        <button data-toggle="modal" data-target="#residenceEditModal" class="btn btn-primary" onClick="residenceEdit('0', '<? print $ID_client;?>');">
						            Ajouter une résidence <i class="fa fa-plus"></i>
						        </button>
						    </div>
						</div>  		    
				    </p>
				    <?
				}
				?>
               </div>
            </section>			
		</div>
	</div>		
	
	<?
}
else
{
	//Fiche inexistante !
	common_header_redirect('Location:clients-liste.php');
}
require("include/footer.inc.php");
?>