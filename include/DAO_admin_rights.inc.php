<?

/*
Protection des pages
*/

function ident_right_check($page_id_action)
{
	if(!is_array($_SESSION['LOGIN']))
	{
		return(false);			
	}
	else
	{
		if($page_id_action == 99 && $_SESSION['LOGIN']['ID_intervenant'] > 0)
		{
			return(false);					
		}
		elseif($_SESSION['LOGIN']['ID_intervenant'] == 0)
		{
			return(true);//Droits ok, on est grand maitre
		}
		else
		{
			$right_to_access 	= 0;
			
			//Plusieurs droits pour une même page
			if( is_array($page_id_action) )
			{
				foreach($page_id_action as $null => $cur_right)
				{
					if(array_search($cur_right, $_SESSION['LOGIN']['admrights']) !== false)
					{
						$right_to_access = 1;
					}
				}
			}
			else
			{
				if(array_search($page_id_action, $_SESSION['LOGIN']['admrights']) !== false)
    			{
    				$right_to_access = 1;
    			}				
			}
			
			if($right_to_access != 1)
			{
				return(false);							
			}
			else
			{
				return(true);//Droits OK
			}
		}
	}
}

function ident_page_protect($page_id_action)
{
	if(!is_array($_SESSION['LOGIN']))
	{
		common_header_redirect('/index.php?err=2');//Vous devez etre identifie
		exit();
	}
	else
	{
		if($page_id_action == 99 && $_SESSION['LOGIN']['ID_intervenant'] > 0)
		{
			common_header_redirect('/index.php?err=3');//Droits insuffisants
			exit();		
		}
		elseif($_SESSION['LOGIN']['ID_intervenant'] == 0)
		{
			return(true);//Droits ok, on est grand maitre
		}
		else
		{
			//Todo verif droits dans BDs
			$right_to_access 	= 0;
			
			//Plusieurs droits pour une même page
			if( is_array($page_id_action) )
			{
				foreach($page_id_action as $null => $cur_right)
				{
					if(array_search($cur_right, $_SESSION['LOGIN']['admrights']) !== false)
					{
						$right_to_access = 1;
					}
				}
			}
			else
			{
				if(array_search($page_id_action, $_SESSION['LOGIN']['admrights']) !== false)
    			{
    				$right_to_access = 1;
    			}				
			}
			
			if($right_to_access != 1)
			{
				common_header_redirect('/index.php?err=3');//Droits insuffisants
				exit();				
			}
			else
			{
				return(true);//Droits OK
			}
		}
	}
}

/*Fonctions cfg_admin_rights (MYSQL_TABLE_CFG_ADMIN_RIGHTS)*/

function cfg_admin_rights_search( $num_start='0', $nb='0', $tri='', $db )
{

	$search_str .= '';

	switch($tri)
	{	
		default:
			$order = "";
		break;
	}
	
	//Nb total de resultats
	if($query = common_mysql_query($db, "SELECT COUNT(ID_admrights) FROM ".MYSQL_TABLE_CFG_ADMIN_RIGHTS." WHERE 1 ".$search_str))
	{
		$nb_resultats_totaux = mysqli_fetch_row($query);
		mysqli_free_result($query);
		$datas['nb_results_total'] = $nb_resultats_totaux[0];	
		
		if($datas['nb_results_total'] > 0 && $nb > 0)
		{
			if($query = common_mysql_query($db, "SELECT DISTINCT(ID_admrights) FROM ".MYSQL_TABLE_CFG_ADMIN_RIGHTS." WHERE 1 ".$search_str." ".$order." LIMIT ".$num_start.",".$nb))
			{
				$datas['nb_results_liste'] = mysqli_num_rows($query);	
				while($datas['results'][] = mysqli_fetch_assoc($query))
				{
					
				}
				return($datas);
			}
			else
			{
				$datas['nb_results_liste'] = 0;
			}		
		
		}
		
	
	}
	else
	{
		$datas['nb_results_total'] = 0;
		$datas['nb_results_liste'] = 0;
	}
	return($datas);

}

function cfg_admin_rights_get_datas($ID_admrights, $db)
{
	if($R1 = common_mysql_query($db, "SELECT * FROM ".MYSQL_TABLE_CFG_ADMIN_RIGHTS." WHERE ID_admrights = '".$ID_admrights."'"))
	{
		$F1 = mysqli_fetch_assoc($R1);
		mysqli_free_result($R1);
		
		return($F1);
	}
	else
	{
		return(false);
	}
}

function cfg_admin_rights_edit( $db, $ID_admrights,$admright_name )
{
	if($ID_admrights == 0)
	{
		common_mysql_query($db, 'INSERT INTO '.MYSQL_TABLE_CFG_ADMIN_RIGHTS." 
							SET admright_name = '".addslashes($admright_name)."'");
	
		return(mysqli_insert_id($db));
	}
	else
	{
		common_mysql_query($db, 'UPDATE '.MYSQL_TABLE_CFG_ADMIN_RIGHTS." 
							SET admright_name = '".addslashes($admright_name)."'
							WHERE ID_admrights = '".$ID_admrights."'");
	
		return($ID_admrights);
	}
						
}

/*Fin cfg_admin_rights (MYSQL_TABLE_CFG_ADMIN_RIGHTS)*/

/*Fonctions cfg_admin_roles (MYSQL_TABLE_CFG_ADMIN_ROLES)*/

function cfg_admin_roles_search( $db, $num_start='0', $nb='0', $tri )
{

	$search_str .= '';

	switch($tri)
	{	
		default:
			$order = "";
		break;
	}
	
	//Nb total de resultats
	if($query = common_mysql_query($db, "SELECT COUNT(ID_admrole) FROM ".MYSQL_TABLE_CFG_ADMIN_ROLES." WHERE 1 ".$search_str))
	{
		$nb_resultats_totaux = mysqli_fetch_row($query);
		mysqli_free_result($query);
		$datas['nb_results_total'] = $nb_resultats_totaux[0];	
		
		if($datas['nb_results_total'] > 0 && $nb > 0)
		{
			if($query = common_mysql_query($db, "SELECT DISTINCT(ID_admrole) FROM ".MYSQL_TABLE_CFG_ADMIN_ROLES." WHERE 1 ".$search_str." ".$order." LIMIT ".$num_start.",".$nb))
			{
				$datas['nb_results_liste'] = mysqli_num_rows($query);	
				while($datas['results'][] = mysqli_fetch_assoc($query))
				{
					
				}
				return($datas);
			}
			else
			{
				$datas['nb_results_liste'] = 0;
			}		
		
		}
		
	
	}
	else
	{
		$datas['nb_results_total'] = 0;
		$datas['nb_results_liste'] = 0;
	}
	return($datas);

}

function cfg_admin_roles_get_datas($db, $ID_admrole)
{
	if($R1 = common_mysql_query($db, "SELECT * FROM ".MYSQL_TABLE_CFG_ADMIN_ROLES." WHERE ID_admrole = '".$ID_admrole."'"))
	{
		$F1 = mysqli_fetch_assoc($R1);
		mysqli_free_result($R1);
		
		return($F1);
	}
	else
	{
		return(false);
	}
}

function cfg_admin_roles_edit( $db, $ID_admrole,$admrole_name, $IDs_admrights)
{
	if($ID_admrole == 0)
	{
		common_mysql_query($db, 'INSERT INTO '.MYSQL_TABLE_CFG_ADMIN_ROLES." 
							SET admrole_name = '".addslashes($admrole_name)."', IDs_admrights = '".addslashes($IDs_admrights)."'");
	
		return(mysql_insert_id());
	}
	else
	{
		common_mysql_query($db, 'UPDATE '.MYSQL_TABLE_CFG_ADMIN_ROLES." 
							SET admrole_name = '".addslashes($admrole_name)."', IDs_admrights = '".addslashes($IDs_admrights)."'
							WHERE ID_admrole = '".$ID_admrole."'");
	
		return($ID_admrole);
	}
						
}

/*Fin cfg_admin_roles (MYSQL_TABLE_CFG_ADMIN_ROLES)*/

/*Fonctions intervenants (MYSQL_TABLE_INTERVENANTS)*/

function intervenants_del($db, $ID_intervenant)
{
	common_mysql_query($db, "DELETE FROM ".MYSQL_TABLE_INTERVENANTS."   WHERE ID_intervenant = '".$ID_intervenant."'");
	
}

function intervenant_exists_login($db, $login)
{
	if($R1 = common_mysql_query($db, "SELECT ID_intervenant FROM ".MYSQL_TABLE_INTERVENANTS." WHERE intervenant_login LIKE '".$login."'"))
	{
		$F1 = mysqli_fetch_assoc($R1);
		mysqli_free_result($R1);
		return($F1['ID_intervenant']);
	}
	else
	{
		return(false);
	}
}

function intervenants_create($db)
{
	common_mysql_query($db, 'INSERT INTO '.MYSQL_TABLE_INTERVENANTS." 
    					SET ID_intervenant = 0");

    return(mysqli_insert_id());
}

function intervenants_update_field($db,  $ID_intervenant, $field_name, $field_val)
{
	return(common_mysql_query($db, 'UPDATE '.MYSQL_TABLE_INTERVENANTS." 
    					SET ".$field_name." = '".addslashes($field_val)."'
    					WHERE ID_intervenant = '".$ID_intervenant."'"));
}

function intervenants_search( $db, $num_start='0', $nb='0', $tri='', $rech_txt='', $type_ressource='', $equipe='', $ID_admrole='')
{

	$search_str .= '';
	
	if($type_ressource != '')
	{
		$search_str .= " AND ressource_type = '".common_mysql_encode($type_ressource)."'";
		/*
		switch($type_ressource)
		{
			case 'infographie':
				$search_str .= " AND ID_admrole = '3'";				
			break;
			case 'homme':
				$search_str .= " AND (ID_admrole = '3' OR ID_admrole = '4' OR ID_admrole = '6' OR ID_admrole = '8')";				
			break;
			case 'pose':
				$search_str .= " AND ID_admrole = '4'";				
			break;
			case 'machine':
				$search_str .= " AND ID_admrole = '5'";				
			break;
		}
		*/
	}
	
	if($equipe != '')
	{
		$search_str .= " AND intervenant_equipe = '".common_mysql_encode($equipe, $db)."'";
	}
	
	if($ID_admrole != '')
	{
		$search_str .= " AND ID_admrole = '".common_mysql_encode($ID_admrole, $db)."'";
	}	
	
	if($rech_txt != '')
	{
		$search_str .= " AND (intervenant_nom LIKE '".common_mysql_encode($rech_txt, $db)."%' OR intervenant_prenom LIKE '".common_mysql_encode($rech_txt, $db)."%' OR intervenant_login '".common_mysql_encode($rech_txt, $db)."%' LIKE OR intervenant_email LIKE '".common_mysql_encode($rech_txt, $db)."%' ) ";
	}	
	
	switch($tri)
	{	
		case 'TRITYPE':
			$order = " ORDER BY intervenant_equipe, ID_admrole, intervenant_nom, intervenant_prenom";
		break;
		case 'TRIEQUIPE':
			$order = " ORDER BY intervenant_equipe, intervenant_nom, intervenant_prenom";
		break;

		default:
			$order = " ORDER BY intervenant_nom, intervenant_prenom";
		break;
	}
	
	//Nb total de resultats
	if($query = common_mysql_query($db, "SELECT COUNT(ID_intervenant) FROM ".MYSQL_TABLE_INTERVENANTS." WHERE 1 ".$search_str))
	{
		$nb_resultats_totaux = mysqli_fetch_row($query);
		mysqli_free_result($query);
		$datas['nb_results_total'] = $nb_resultats_totaux[0];	
		
		if($datas['nb_results_total'] > 0 && $nb > 0)
		{
			if($query = common_mysql_query($db, "SELECT DISTINCT(ID_intervenant) FROM ".MYSQL_TABLE_INTERVENANTS." WHERE 1 ".$search_str." ".$order." LIMIT ".$num_start.",".$nb))
			{
				$datas['nb_results_liste'] = mysqli_num_rows($query);	
				while($datas['results'][] = mysqli_fetch_assoc($query))
				{
					
				}
				return($datas);
			}
			else
			{
				$datas['nb_results_liste'] = 0;
			}		
		
		}
		
	
	}
	else
	{
		$datas['nb_results_total'] = 0;
		$datas['nb_results_liste'] = 0;
	}
	return($datas);

}

function intervenants_get_datas($db, $ID_intervenant)
{
	if($R1 = common_mysql_query($db, "SELECT * FROM ".MYSQL_TABLE_INTERVENANTS." WHERE ID_intervenant = '".$ID_intervenant."'"))
	{
		$F1 = mysqli_fetch_assoc($R1);
		mysqli_free_result($R1);
		
		return($F1);
	}
	else
	{
		return(false);
	}
}

/*Fin intervenants (MYSQL_TABLE_INTERVENANTS)*/