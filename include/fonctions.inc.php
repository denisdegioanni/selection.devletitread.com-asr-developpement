<?
/****************
* NOUVEAU PROJET *
* Fonctions *
****************/

require(dirname(__FILE__).'/DAO_admin_rights.inc.php');
require(dirname(__FILE__).'/DAO_clients.inc.php');
require(dirname(__FILE__).'/DAO_residences.inc.php');




/*mySQL*/
function common_get_extension($filename)
{
	$filearr = explode('.', $filename);
	$nbelems = (sizeof($filearr)-1);
	$ext     = $filearr[$nbelems];
	
	return($ext);
}

function common_remove_accents($str, $charset='utf-8')
{
    $str = htmlentities($str, ENT_NOQUOTES, $charset);
    
    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caract�res
    
    return $str;
}

function common_get_weekday_fr($date)
{
	switch(date('w', strtotime($date)))
	{
		case 0:
			return('Dim');
		break;	
		case 1:
			return('Lun');
		break;	
		case 2:
			return('Mar');
		break;	
		case 3:
			return('Mer');
		break;	
		case 4:
			return('Jeu');
		break;	
		case 5:
			return('Ven');
		break;	
		case 6:
			return('Sam');
		break;	
	}
}

function common_get_monday_of_week($date)
{
	$curday = date('w', strtotime($date));
	
	switch($curday)
	{
		case 0:
			return(date('Y-m-d', mktime(0,0,0, date('m', strtotime($date)),date('d', strtotime($date))-6, date('Y', strtotime($date)) )));
		break;	
		case 1:
			return(date('Y-m-d', strtotime($date)));
		break;	
		case 2:
			return(date('Y-m-d', mktime(0,0,0, date('m', strtotime($date)),date('d', strtotime($date))-1, date('Y', strtotime($date)) )));
		break;	
		case 3:
			return(date('Y-m-d', mktime(0,0,0, date('m', strtotime($date)),date('d', strtotime($date))-2, date('Y', strtotime($date)) )));
		break;	
		case 4:
			return(date('Y-m-d', mktime(0,0,0, date('m', strtotime($date)),date('d', strtotime($date))-3, date('Y', strtotime($date)) )));
		break;	
		case 5:
			return(date('Y-m-d', mktime(0,0,0, date('m', strtotime($date)),date('d', strtotime($date))-4, date('Y', strtotime($date)) )));
		break;	
		case 6:
			return(date('Y-m-d', mktime(0,0,0, date('m', strtotime($date)),date('d', strtotime($date))-5, date('Y', strtotime($date)) )));
		break;			
	}
}

function common_js_sautdeligne($str)
{
	$str = str_replace("\r\n", "\\n", addslashes($str));
	
	return($str);
}
 	
function common_mysql_connect($db_name)
{
	$db = mysqli_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PWD) or DIE(err_log("Connexion MySQL ".MYSQL_USER."@".MYSQL_HOST." Impossible", "Pb connect ".MYSQL_USER."@".MYSQL_HOST));
	//mysql_select_db($db_name, $db) or DIE(err_log("Selection BD MySQL ".$db_name." Impossible", "Pb select db ".$db_name." sur ".MYSQL_USER."@".MYSQL_HOST));
	mysqli_select_db($db, $db_name) or DIE("Erreur selection BD (session termin�e)");
	return($db);
}

function common_mysql_query($db_link, $query) 
{
	
	$r1 = mysqli_query($db_link, $query) or DIE(err_log("Requete MySQL Erreur", "common_mysql_query($query) - Erreur : ".mysqli_error($db_link)));
	if(@mysqli_num_rows($r1) > 0)
	{
		return($r1);		
	}
	else
	{
		return(false);
	}

}

function common_mysql_encode($db_link, $variable, $auth_html=0)
{
    if(get_magic_quotes_gpc())
    {
        $variable = stripslashes($variable);
    }

    if($auth_html != 1)
    {
    	$variable = strip_tags($variable);
    }

    $variable = mysqli_real_escape_string($db_link, $variable);

    return $variable;
}

/*encodage d'une chaine en sql*/
function common_sqlencode($string, $db_link)
{
	$r1 = mysqli_query($db_link, "SELECT PASSWORD('".addslashes($string)."')");
	$f1 = mysqli_fetch_row($r1);
	return($f1[0]);
}

function common_csvencode($string, $sepateur, $protection)
{
	$string = str_replace($sepateur, ',', $string);
	$string = str_replace($protection, '', $string);
	return($string);
}


function common_sql_query_from_array($query_type, $table_name, $array_vals, $where='1')
{
	switch($query_type)
	{
		case 'UPDATE':
		
			$sql_query = 'UPDATE '.$table_name.' SET ';
			foreach($array_vals AS $fields => $vals)
			{
				if(substr($vals, 0,4) == 'SQL:')
				{
					//C'est une expression SQL, on envoie comme tel
					$sql_query .= $fields." = ".substr($vals,4);
				}
				else
				{
					$sql_query .= $fields." = '".addslashes($vals)."'";
				}
				$id ++;
				if($id < sizeof($array_vals))
				{
					$sql_query .= ',';
				}
				
			}			
			$sql_query .= ' WHERE '.$where;
		
		break;
		
		case 'INSERT':
		
			$sql_query = 'INSERT INTO '.$table_name.'(';
			foreach($array_vals AS $fields => $vals)
			{
				$sql_query .= $fields;
				$id ++;
				if($id < sizeof($array_vals))
				{
					$sql_query .= ',';
				}
			}	
			$sql_query .= ') VALUES(';
			$id = 0;			
			reset($array_vals);
			foreach($array_vals AS $fields => $vals)
			{
				if(substr($vals, 0,4) == 'SQL:')
				{
					//C'est une expression SQL, on envoie comme tel
					$sql_query .= substr($vals,4);
				}
				else
				{
					$sql_query .= "'".addslashes($vals)."'";
				}
				$id ++;
				if($id < sizeof($array_vals))
				{
					$sql_query .= ',';
				}
			}	

			$sql_query .= ')';
		
		break;	

	}
	return ($sql_query);

}


function common_js_check_field($champobli_arr, $target='parent')
{

	    /*
	    Ex :
	    $champ_obli[]   = 'login,str,4,20';
	    $champ_obli[]   = 'pwd_1,str,4,20';
	    $champ_obli[]   = 'pwd_1,equ,pwd_2';
	    $champ_obli[]   = 'rs,str,1,100';
	    $champ_obli[]   = 'capital,int,1,10';
	    $champ_obli[]   = 'adresse,str,5,255';
	    $champ_obli[]   = 'cpost,str,5,6';
	    $champ_obli[]   = 'ville,str,2,100';
	    $champ_obli[]   = 'nom,str,2,100';
	    $champ_obli[]   = 'prenom,str,2,100';
	    $champ_obli[]   = 'email,eml,0';
	    $champ_obli[]   = 'email,equ,email2';
	    $champ_obli[]   = 'tel,str,8,15';
	    */
		foreach($champobli_arr as $id => $verif_datas)
		{
		 			$verif_datas_arr = explode(',', $verif_datas);
					$field_name      = $verif_datas_arr[0];
					$verif_type      = $verif_datas_arr[1];
					$verif_val       = $verif_datas_arr[2];
					$verif_val2      = $verif_datas_arr[3];
										
					switch($verif_type)
					{
					    case 'str':
									 //Chaine
									 $field_value = trim($_REQUEST[$field_name]);
									 if(strlen($field_value) < $verif_val || strlen($field_value) > $verif_val2)
									 {
									 	$err[$field_name] = 'Valeur incorrecte (de '.$verif_val.' &agrave; '.$verif_val2.' car.)';
									 }
									 else
									 {
									 	 $ok[$field_name]	  = 1; 								 
									 }

						break;

					    case 'int':
									 //Entier
									 $field_value = trim($_REQUEST[$field_name]);
									 if(strlen($field_value) < $verif_val || strlen($field_value) > $verif_val2)
									 {
									 		$err[$field_name] = 'Valeur incorrecte (de '.$verif_val.' &agrave; '.$verif_val2.' car.)';
									 }				
									 elseif(!is_numeric($field_value))
									 {
									 		$err[$field_name] = 'Valeur incorrecte (num&eacute;rique seulement)';
									 }				
									 else
									 {
									 	 $ok[$field_name]	  = 1; 								 
									 }
							
						break;
							
					    case 'eml':
									 //Email
									 $field_value = trim($_REQUEST[$field_name]);
									 if(strpos($field_value, '@') > 0 && strpos($field_value, '.') > 0)
									 {
									 	$ok[$field_name]	  = 1; 		
									 }				
									 else
									 {
									 	$err[$field_name] = 'E-mail incorrect';						 
									 }
							
						break;							
							
					    case 'equ':
									 //Equivalence deux champs
									 $field_value = trim($_REQUEST[$field_name]);
									 if($field_value != $_REQUEST[$verif_val])
									 {
									 		$err[$field_name] = 'Valeurs non correspondantes';
									 }
									 else
									 {
									 	 $ok[$field_name]	  = 1; 								 
									 }
						break;	

						case 'chkbox':
									 //Equivalence deux champs
									 $field_value = sizeof($_REQUEST[$field_name]);
									 if($field_value < $verif_val || $field_value > $verif_val2)
									 {
									 	$err[$field_name] = 'Incorrect : de '.$verif_val.' &agrave; '.$verif_val2.' choix';
									 }
									 else
									 {
									 	 $ok[$field_name]	  = 1; 								 
									 }
						break;

						case 'obli':
									 //Equivalence deux champs
									 $field_value = sizeof($_REQUEST[$field_name]);
									 if($field_value == '')
									 {
									 	$err[$field_name] = 'Champ obligatoire';
									 }
									 else
									 {
									 	 $ok[$field_name]	  = 1; 								 
									 }
						break;

						case 'file':
									 //Equivalence deux champs
									if ($_FILES[$field_name]['error']) 
									{
											switch ($_FILES[$field_name]['error'])
											{
											    case 1: // UPLOAD_ERR_INI_SIZE
												   $err[$field_name] = "Fichier trop lourd";
											    break;
											    case 2: // UPLOAD_ERR_FORM_SIZE
												   $err[$field_name] = "Fichier trop lourd !";
											    break;
											    case 3: // UPLOAD_ERR_PARTIAL
												   $err[$field_name] = "L'envoi du fichier a �t� interrompu pendant le transfert !";
											    break;
											    case 4: // UPLOAD_ERR_NO_FILE
												if($verif_val == 1)//Si le fichier est obligatoire, erreur
												{
												   $err[$field_name] = "Le fichier que vous avez envoy� a une taille nulle !";
												}
												else
												{
													$ok[$field_name]	  = 1; 								 												
												}
											    break;
											}
									}
									else {
										//Test extension si necessaire
										if($verif_val2 != '')
										{
											$file_ext = common_get_extension($_FILES[$field_name]['name']);
											if(strpos($verif_val2, $file_ext) === false)
											{
												$err[$field_name] = "Type de fichier incorrect";
											}
											else
											{
												$ok[$field_name]	  = 1;
											}
										}
										else
										{
											$ok[$field_name]	  = 1; 				
										}
									 					 
									}
						break;
					}
		}
		

  	if(is_array($ok))
  	{
	 		$chaine_verif .= '<script language="JavaScript" type="text/javascript">'."\n";
  
			foreach( $ok as $field_name => $msg )
			{
				$chaine_verif .= $target.'.$("'.$field_name.'Err").update("");'."\n";
				$chaine_verif .= $target.'.$("'.$field_name.'").className = "";'."\n";
			}
  			
  			$chaine_verif .= '</script>'."\n";
		
	  }
  	if(is_array($err))
  	{
	 		$chaine_verif .= '<script language="JavaScript" type="text/javascript">'."\n";
  
			foreach( $err as $field_name => $msg )
			{
				$chaine_verif .= $target.'.$("'.$field_name.'Err").update("'.$msg.'");'."\n";
				$chaine_verif .= $target.'.$("'.$field_name.'Err").className = "error";'."\n";
				$chaine_verif .= $target.'.$("'.$field_name.'").className = "error";'."\n";
			}
  			
  			$chaine_verif .= '</script>'."\n";

  			$chaine_verif_datas['status'] = false;
  	}
  	else
  	{
				$chaine_verif_datas['status'] = true;
  	}		
		$chaine_verif_datas['js']     = $chaine_verif;
  	return($chaine_verif_datas);
}

//Redirection propre
function common_header_redirect($url_to_redir)
{
	if (!headers_sent($filename, $linenum)) {
	    header('Location:'.$url_to_redir);
	    exit;
	
	// You would most likely trigger an error here.
	} else {
		echo "<script>window.location='".$url_to_redir."';</script>";
	    echo "Merci de <a href=\"".$url_to_redir."\">cliquer sur ce lien pour acc�der au contenu demand�.</a>\n";
	    exit;
	}
}

/*images*/
function common_jpg_resize ($source_im, $dest_im, $size_im, $quality=80)
{
		$type = getimagesize($source_im);
		
		if($type[2] == "2")
		{
				$image  = ImageCreateFromJPEG($source_im);
				$width  = ImageSX($image) ;
				$height = ImageSY($image) ;
	
				if($width > $height)
					$grand_cote = "width";
				else
					$grand_cote = "height";
	
				//Largeur est le plus grand cote
				$new_width  = $size_im; // largeur a definir
				$new_height = $size_im; // hauteur a definir
	
				$thumb = ImageCreateTrueColor($new_width,$new_height);
			
				ImageCopyResampled($thumb,$image,0,0,0,0,$new_width,$new_height,$width,$height);
	
				ImageJPEG($thumb, $dest_im, $quality);
				ImageDestroy($image);
				
				return (true);
		}
		else
		{
			return (false);
		}
}

function common_jpg_resize_wh ($source_im, $dest_im, $size_im_w, $size_im_h, $quality=80)
{
		$type = getimagesize($source_im);
		
		if($type[2] == "2")
		{
				$image  = ImageCreateFromJPEG($source_im);
				$width  = ImageSX($image) ;
				$height = ImageSY($image) ;

				$new_width  = $size_im_w; // largeur a definir
				$new_height = $size_im_h; // hauteur a definir
	
				$thumb = ImageCreateTrueColor($new_width,$new_height);
			
				ImageCopyResampled($thumb,$image,0,0,0,0,$new_width,$new_height,$width,$height);
	
				ImageJPEG($thumb, $dest_im, $quality);
				ImageDestroy($image);
				
				return (true);
		}
		else
		{
			return (false);
		}
}

function common_jpg_resize_width ($source_im, $dest_im, $size_im, $quality=80)
{
		$type = getimagesize($source_im);
		
		if($type[2] == "2")
		{
				$image  = ImageCreateFromJPEG($source_im);
				$width  = ImageSX($image) ;
				$height = ImageSY($image) ;
	
  				$new_width  = $size_im; // largeur a definir
  				$new_height = ($new_width * $height) / $width ; // hauteur proportionnelle
  
				$thumb = ImageCreateTrueColor($new_width,$new_height);
			
				ImageCopyResampled($thumb,$image,0,0,0,0,$new_width,$new_height,$width,$height);
	
				ImageJPEG($thumb, $dest_im, $quality);
				ImageDestroy($image);
				
				return (true);
		}
		else
		{
			return (false);
		}
}

function common_jpg_resize_height ($source_im, $dest_im, $size_im, $quality=80)
{
		$type = getimagesize($source_im);
		
		if($type[2] == "2")
		{
				$image  = ImageCreateFromJPEG($source_im);
				$width  = ImageSX($image) ;
				$height = ImageSY($image) ;
	
  				$new_height  = $size_im; // largeur a definir
  				$new_width = ($new_height * $width) / $height ; // hauteur proportionnelle
  
				$thumb = ImageCreateTrueColor($new_width,$new_height);
			
				ImageCopyResampled($thumb,$image,0,0,0,0,$new_width,$new_height,$width,$height);
	
				ImageJPEG($thumb, $dest_im, $quality);
				ImageDestroy($image);
				
				return (true);
		}
		else
		{
			return (false);
		}
}

function common_calcul_age($jour, $mois, $annee)
{
	$naiss = mktime(0, 0, 0, $mois, $jour, $annee);
	$today = mktime();
	$secondes = ($today > $naiss)? $today - $naiss : $naiss - $today;
	
	$annees = date('Y', $secondes) - 1970;
	
	return($annees);
}

function common_sql_date_fr($sql_date)
{
	$date = explode(' ', $sql_date);
	$elem = explode('-', $date[0]);

	$french = $elem[2].'/'.$elem[1].'/'.$elem[0];
	
	return($french);
}

function common_sql_date_nosigne($sql_date)
{
	$sql_date = str_replace('-', '', $sql_date);
	$sql_date = str_replace(' ', '', $sql_date);
	$sql_date = str_replace(':', '', $sql_date);
	
	return($sql_date);
}

function common_sql_date_get_hourmin($sql_date)
{
	$date = explode(' ', $sql_date);
	$elem = explode(':', $date[1]);

	$hourmin = $elem[0].':'.$elem[1];
	
	return($hourmin);
}

function common_fr_date_sql($fr_date)
{
	$date = explode('/', $fr_date);

	$sql  = $date[2].'-'.$date[1].'-'.$date[0];
	
	return($sql);
}

function common_select_num($num_start, $num_end, $num_selected, $zero_av_dix=0)
{
  for($i=$num_start;$i <= $num_end; $i ++)
	{
	   if( $i < 10 && $zero_av_dix == 1)
		 {
		 		 $num_aff = '0'.$i;
		 }
		 else
		 {
		 		 $num_aff = $i;
		 }
		 print '<option value="'.$num_aff.'"';
		 if($num_aff == $num_selected)
		 {
		 				print ' selected';
		 }
		 print '>'.$num_aff.'</option>';
	}
}

//Nettoie une query string de plusieurs criteres (utile pour tri), separes par |
function common_rewrite_query_string($criteres_a_enlever)
{
	GLOBAL $_GET;
	
	$criteres_a_enlever_arr = explode('|', $criteres_a_enlever);
	
	foreach( $_GET as $key => $value )
	{
		if( array_search($key, $criteres_a_enlever_arr) === false )
		{
			$query_string .= '&'.$key.'='.$value;
		}
	}
	
	return($query_string);
}

/*chaines de caracteres*/
function str_coupure_trolong($string,$length,$separation=' ')
{
	return preg_replace('/([^ ]{'.$length.'})/si','\1'.$separation,$string);
}

function str_csv_encode($separateur, $car2remplacement, $txt)
{
	return (str_replace($separateur, $car2remplacement, $txt));
}

function str_alphanum_valid($str_pseu)
{
	for($i=0 ; $i<strlen($str_pseu) ; $i++)
	{
  		$car = substr($str_pseu,$i,1);
  		if((ord($car) < 32 //en dessous de Espace
  		or (ord($car) > 32 and ord($car) <= 38)//!"#$%&
  		or (ord($car) > 38 and ord($car) < 48)//'()*+,-./
  		or (ord($car) > 57 and ord($car) < 65)//:;<=>?@
  		or (ord($car) > 90 and ord($car) < 97)//[\]^_`
  		or ord($car) > 122)) //{|}~
		{
		 	 return (false);
		}
	}	
	return(true);
}

function str_txt_format($str)
{
	$txt = strtolower($str);
	$txt = ucfirst($txt);
	$txt = $txt;
	return($txt);
}

function str_pubpage_pseudo_enc($str)
{

	return(urlencode($str));
}


function str_truncate($str, $length, $notitle=0)
{
	if(strlen($str) > $length)
	{
		if($notitle == 1)
		{
			$str = substr($str, 0, $length).'...';
		}
		else
		{
			$str = '<span title="'.strip_tags($str).'">'.substr($str, 0, $length).'...</span>';
		}
	}
	return($str);
}


function str_money_format($str)
{
	$nombre_format_francais = number_format($str, 2, ',', ' ');
	return($nombre_format_francais);
}

//Fonction transformant un intervale en secondes en temps mm:ss
function str_seconds_to_ms($sec_str)
{
	if($sec_str > 59)
	{
		$nb_min = floor($sec_str/60);
	}
	else
	{
		$nb_min = "0";
	}
	
	if($nb_min < 10)
	{
		$nb_min = "0".$nb_min;
	}
	
	$nb_sec = $sec_str-($nb_min*60);
	if($nb_sec < 10)
	{
		$nb_sec = '0'.$nb_sec;
	}
		
	$resultat = $nb_min.':'.$nb_sec;
	
	return ($resultat);
}

function str_email_valid($email)
{
   	 if(!eregi("^[[:alpha:]]{1}[[:alnum:]]*((\.|_|-)[[:alnum:]]+)*@[[:alpha:]]{1}[[:alnum:]]*((\.|-)[[:alnum:]]+)*(\.[[:alpha:]]{2,})$", $email))
	 {
      		//echo('L\'adresse '.$Email.' est mal format�e');
			return(false);
	 }
	 else
	 {
			return(true);
	 }
}

function str_random($car) {

	$string = "";
	$chaine = "abcdefghijklmnpqrstuvwxyz";
	srand((double)microtime()*1000000);
	for($i=0; $i<$car; $i++) 
	{
		$string .= $chaine[rand()%strlen($chaine)];
	}
	return $string;

}

/*error log*/
function err_log($subj, $txt)
{
	if($_SERVER['REMOTE_ADDR'] == CFG_DEBUG_IP)
	{
		print ' ERR : '.$subj.':'.$txt;
	}
	$request = print_r($_REQUEST, true);
	$session = print_r($_SESSION, true);
	
    $txt = "Hote : ".$_SERVER['HTTP_HOST']."\nIP ".$_SERVER['REMOTE_ADDR']."\nNav : ".$_SERVER['HTTP_USER_AGENT']."\nPage : ".$_SERVER['PHP_SELF']."\nQuerystring : ".$_SERVER['QUERY_STRING']."\nRequest :\n--- START ---\n".$request."\n--- END ---\Session :\n--- START ---\n".$session."\n--- END ---\nMessage : ".$txt;

	mail(CFG_SITE_DBUG_ML, CFG_SITE_NAMESHORT.' ERRLOG : '.$subj, $txt);
}

/*emails*/
function email_send($dest ,$subject, $txt, $exp='')
{
	if($exp == '')
	{
		$exp = CFG_SITE_MAIL;
	}
	$headers = 'From: '.CFG_SITE_NAMELONG.'<'.$exp.'>' . "\r\n" . 'Reply-To: '.$exp. "\r\n";
	mail($dest, $subject, $txt, $headers) or DIE(err_log("Fonction email_send", "Envoi mail ".$subject));
}

function email_send_html ($from, $from_name, $to, $replyto, $subject, $content_html, $content_txt="") 
{		
		$header = "From: $from_name <$from>\n";
		$header .="Reply-To: $from_name <$replyto>\n";
		$header .="MIME-Version: 1.0\n";

		if ( $content_txt == "" || eregi("@hotmail.", $to) || eregi("@wanadoo.", $to))
		{
    		$header .= "Content-Type: text/html; charset=iso-8859-1\n";
    		$content = $content_html;
		}		
		else 
		{
				
		//Envoi mail format HTML + Txt
		
		$limite = "_parties_".md5 (uniqid (rand())); 
		
		$header .= "Content-Type: multipart/alternative;\n"; 
		$header .= " boundary=\"----=$limite\"\n";
				
		//Format TXT
        $texte_simple = "This is a multi-part message in MIME format.\n"; 
        $texte_simple .= "Ceci est un message est au format MIME.\n"; 
        $texte_simple .= "------=$limite\n"; 
        $texte_simple .= "Content-Type: text/plain; charset=iso-8859-1\n"; 
        $texte_simple .= $content_txt; 

        //Format HTML
        $texte_html = "------=$limite\n"; 
        $texte_html .= "Content-Type: text/html; charset=iso-8859-1\n"; 
        $texte_html .= $content_html; 
        $texte_html .= "\n\n\n------=$limite\n";				
				
				
				$content = $texte_simple.$texte_html."-- End --";

		}
		
		mail($to, $subject, $content, $header);	
}

