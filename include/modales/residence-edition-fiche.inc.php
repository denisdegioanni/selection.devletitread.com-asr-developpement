								<div class="modal fade" id="residenceEditModal" tabindex="-1" role="dialog" aria-labelledby="residenceEditModalLabel" aria-hidden="true">
								    <div class="modal-dialog">
								    	<div class="modal-content">
								    		<div class="modal-header">
								    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								    			<h4 class="modal-title" id="residenceEditFormLabel">
								    				Edition résidence
								    			</h4>
								    		</div>
											<form autocomplete="off" action="residence-edition-fiche-process.php" type="post" id="residenceEditForm" submit-checkfields="1" submit-ajax="1" submit-checkfields-error="residenceEditModalErrField">
												<input type="hidden" name="errfield" value="residenceEditModalErrField" class="form-control">
												<input type="hidden" name="ID_residence" value="0" class="form-control">
												<input type="hidden" name="ID_client" value="0" class="form-control">
												
									    		<div class="modal-body">
										    		
										    		<h4>
											    		Informations générales
										    		</h4>
												    											    											    								    		
												    <div class="row">													   
    											        <div class="form-group">
    											        	<div class="col-md-12">
    											        		<label>Nom</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																    <input name="nom" mandatory_type="string" mandatory_value="1" placeholder="Nom" mandatory_fielderr="Merci de saisir un nom correct" type="text" value="" maxlength="150" class="form-control">
																</div>    											        		
    											        		
    											        	</div>	
    											        </div>
												    </div>											    											    										    
												    <div class="row">													   
    											        <div class="form-group">
    											        	<div class="col-md-12">
    											        		<label>Adresse</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																    <input name="adresse" placeholder="Adresse postale" type="text" value="" maxlength="255" class="form-control">
																</div>    											        		
    											        		
    											        	</div>	
    											        </div>
												    </div>		
												    <div class="row">													   
    											        <div class="form-group">
    											        	<div class="col-md-12">
    											        		<label>Adresse  (suite)</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																    <input name="adresse_suite" placeholder="Adresse postale (suite)" type="text" value="" maxlength="255" class="form-control">
																</div>    											        		
    											        		
    											        	</div>	
    											        </div>
												    </div>	
												    <div class="row">													   
    											        <div class="form-group">
    											        	<div class="col-md-6">
    											        		<label>Code Postal</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																    <input name="cpost" placeholder="Code postal" type="text" value="" maxlength="10" class="form-control" mandatory_type="string" mandatory_value="1" mandatory_fielderr="Merci de saisir un code postal correct" >
																</div>    											        		
    											        	</div>	
															<div class="col-md-6">
    											        		<label>Ville</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																    <input name="ville" placeholder="Ville" type="text" value="" maxlength="100" class="form-control" mandatory_type="string" mandatory_value="1" mandatory_fielderr="Merci de saisir une ville correcte" >
																</div>    											        		
    											        		
    											        	</div>	    											        	
    											        </div>
												    </div>	
												    <div class="row">													   
    											        <div class="form-group">
    											        	<div class="col-md-12">
    											        		<label>Pays</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																	<select class="form-control" name="pays">
																	    <?
																		foreach($MYSQL_ENUM_DATA_RESIDENCE_PAYS as $id => $text)
																	    {
																			?>
																			<option value="<? print $id;?>"><? print $text;?></option>
																			<?    																        	
																		}
																	    ?>
																	</select>
																</div>    											        		
    											        		
    											        	</div>	
    											        </div>
												    </div>

											    
		    			
									    			<div id="residenceEditModalErrField"></div>	
									    		</div>
								    		
									    		<div class="modal-footer">
									    			<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
									    			<input type="submit" value="Enregistrer" class="btn btn-primary" data-loading-text="Chargement...">
									    		</div>
											</form>
								    	</div>
								    </div>
								</div>