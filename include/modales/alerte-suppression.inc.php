								<div class="modal fade" id="confirmDeleteModal" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
								    <div class="modal-dialog">
								    	<div class="modal-content">
								    		<div class="modal-header">
								    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								    			<h4 class="modal-title" id="confirmDeleteLabel">
								    				Confirmer suppression ?
								    			</h4>
								    		</div>
								    		<div class="modal-body">

											    <div class="row">													   
											    	Confirmez vous la suppression de cet élément ? Cette suppression sera définitive.
											    </div>
								    		
									    		<div class="modal-footer">
									    			<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
									    			<input type="button" id="confirmDeleteBtn" value="Confirmer" class="btn btn-primary">
									    		</div>

											</form>
								    	</div>
								    </div>
								</div>