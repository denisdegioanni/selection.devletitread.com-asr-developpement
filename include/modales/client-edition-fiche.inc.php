								<div class="modal fade" id="clientEditModal" tabindex="-1" role="dialog" aria-labelledby="clientEditModalLabel" aria-hidden="true">
								    <div class="modal-dialog">
								    	<div class="modal-content">
								    		<div class="modal-header">
								    			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								    			<h4 class="modal-title" id="clientEditFormLabel">
								    				Edition client
								    			</h4>
								    		</div>
											<form autocomplete="off" action="client-edition-fiche-process.php" type="post" id="clientEditForm" submit-checkfields="1" submit-ajax="1" submit-checkfields-error="clientEditModalErrField">
												<input type="hidden" name="errfield" value="clientEditModalErrField" class="form-control">
												<input type="hidden" name="ID_client" value="0" class="form-control">
												
									    		<div class="modal-body">
										    		
										    		<h4>
											    		Informations générales
										    		</h4>
												    <div class="row">													   
    											        <div class="form-group">
    											        	<div class="col-md-6">
    											        		<label>Adresse e-mail</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																    <input name="email" placeholder="Adresse e-mail" type="text" value="" maxlength="255" mandatory_type="email" mandatory_value="1" mandatory_fielderr="Merci de saisir une adresse e-mail correcte" class="form-control">
																</div>    											        		
    											        	</div>	
    											        	<div class="col-md-6">
    											        		<label>Mot de passe backoffice</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																    <input name="upd_pwd" placeholder="Modifier mot de passe backoffice (facultatif)" type="password" value="" maxlength="20" class="form-control">
																</div>    											        		
    											        		
    											        	</div>    											        	
    											        </div>
												    </div>				
										    		<h4>
											    		Informations complémentaires
										    		</h4>												    										    		
												    <div class="row">													   
    											        <div class="form-group">
    											        	<div class="col-md-12">
    											        		<label>Type</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																	<select class="form-control" name="type">
																	    <?
																		foreach($MYSQL_ENUM_DATA_CLIENT_TYPE as $id => $text)
																	    {
																			?>
																			<option value="<? print $id;?>"><? print $text;?></option>
																			<?    																        	
																		}
																	    ?>
																	</select>
																</div>    											        		
    											        		
    											        	</div>	
    											        </div>
												    </div>	
												    <div class="row">			
    											        <div class="form-group">
    											        	<div class="col-md-12">
    											        		<label>Client depuis</label>
																<div class="input-group m-bot15 input-append date dpYears" data-date-viewmode="years" data-date-format="dd/mm/yyyy" data-date="">
																	<input name="date_depuis" mandatory_type="date" mandatory_value="1" mandatory_fielderr="Merci de saisir une date d'entrée en relation correcte" placeholder="Client depuis date (DD/MM/AAAA)" type="text" value="" maxlength="50" class="form-control">
																	<span class="input-group-btn add-on">
																	  <button class="btn btn-primary" type="button"><i class="fa fa-calendar"></i></button>
																	</span>																		
																</div>    											        		
    											        	</div>	
    											        </div>												        
												    </div>												    											    											    								    		
												    <div class="row">													   
    											        <div class="form-group">
    											        	<div class="col-md-12">
    											        		<label>Raison Sociale</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																    <input name="rs" mandatory_type="string" mandatory_value="1" placeholder="Raison Sociale" mandatory_fielderr="Merci de saisir une raison sociale correcte" type="text" value="" maxlength="150" class="form-control">
																</div>    											        		
    											        		
    											        	</div>	
    											        </div>
												    </div>											    											    										    
												    <div class="row">													   
    											        <div class="form-group">
    											        	<div class="col-md-12">
    											        		<label>Adresse</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																    <input name="adresse" placeholder="Adresse postale" type="text" value="" maxlength="255" class="form-control">
																</div>    											        		
    											        		
    											        	</div>	
    											        </div>
												    </div>		
												    <div class="row">													   
    											        <div class="form-group">
    											        	<div class="col-md-12">
    											        		<label>Adresse  (suite)</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																    <input name="adresse_suite" placeholder="Adresse postale (suite)" type="text" value="" maxlength="255" class="form-control">
																</div>    											        		
    											        		
    											        	</div>	
    											        </div>
												    </div>	
												    <div class="row">													   
    											        <div class="form-group">
    											        	<div class="col-md-6">
    											        		<label>Code Postal</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																    <input name="cpost" placeholder="Code postal" type="text" value="" maxlength="10" class="form-control" mandatory_type="string" mandatory_value="1" mandatory_fielderr="Merci de saisir un code postal correct" >
																</div>    											        		
    											        	</div>	
															<div class="col-md-6">
    											        		<label>Ville</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																    <input name="ville" placeholder="Ville" type="text" value="" maxlength="50" class="form-control" mandatory_type="string" mandatory_value="1" mandatory_fielderr="Merci de saisir une ville correcte" >
																</div>    											        		
    											        		
    											        	</div>	    											        	
    											        </div>
												    </div>	
												    <div class="row">													   
    											        <div class="form-group">
    											        	<div class="col-md-12">
    											        		<label>Pays</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																	<select class="form-control" name="pays">
																	    <?
																		foreach($MYSQL_ENUM_DATA_CLIENT_PAYS as $id => $text)
																	    {
																			?>
																			<option value="<? print $id;?>"><? print $text;?></option>
																			<?    																        	
																		}
																	    ?>
																	</select>
																</div>    											        		
    											        		
    											        	</div>	
    											        </div>
												    </div>												    
												    <div class="row">													   
    											        <div class="form-group">
    											        	<div class="col-md-12">
    											        		<label>E-Mail dédié</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																    <input name="email_dedie" placeholder="Adresse e-mail dédiée" type="text" value="" maxlength="255" class="form-control">
																</div>    											        		
    											        		
    											        	</div>	
    											        </div>
												    </div>
												    <div class="row">													   
    											        <div class="form-group">
    											        	<div class="col-md-12">
    											        		<label>Zone</label>
																<div class="input-group m-bot15">
																    <span class="input-group-addon btn-white"><i class="fa fa-user"></i></span>
																	<select class="form-control" name="zone">
																	    <?
																		foreach($MYSQL_ENUM_DATA_CLIENT_ZONE as $id => $text)
																	    {
																			?>
																			<option value="<? print $id;?>"><? print $text;?></option>
																			<?    																        	
																		}
																	    ?>
																	</select>
																</div>    											        		
    											        		
    											        	</div>	
    											        </div>
												    </div>												    
		    			
									    			<div id="clientEditModalErrField"></div>	
									    		</div>
								    		
									    		<div class="modal-footer">
									    			<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
									    			<input type="submit" value="Enregistrer" class="btn btn-primary" data-loading-text="Chargement...">
									    		</div>
											</form>
								    	</div>
								    </div>
								</div>