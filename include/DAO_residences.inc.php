<?php
/*
	FONCTION D'ACCES AUX DONNEES Tables th_residences + th_residences_contacts
*/	
	
	
/*Fonctions th_residences (MYSQL_TABLE_TH_RESIDENCES)*/

function th_residences_search( $db, $num_start='0', $nb='0', $tri='', $select_adds_fields='', $ID_client='' )
{

	$search_str = " AND residence_status = 'ACTIF'";

	if($ID_client != '')
	{
		$search_str .= " AND ID_client = '".$ID_client."'";		
	}	

	switch($tri)
	{	
		default:
			$order = " ORDER BY residence_nom, residence_batiment, residence_entree";
		break;
	}
	
	//Nb total de resultats
	if($query = common_mysql_query($db, "SELECT COUNT(ID_residence) FROM ".MYSQL_TABLE_TH_RESIDENCES." INNER JOIN ".MYSQL_TABLE_TH_CLIENTS." USING(ID_client) WHERE 1 ".$search_str))
	{
		$nb_resultats_totaux = mysqli_fetch_row($query);
		mysqli_free_result($query);
		$datas['nb_results_total'] = $nb_resultats_totaux[0];	
		
		if($datas['nb_results_total'] > 0 && $nb > 0)
		{
			if($query = common_mysql_query($db, "SELECT DISTINCT(ID_residence)".$select_adds_fields." FROM ".MYSQL_TABLE_TH_RESIDENCES." INNER JOIN ".MYSQL_TABLE_TH_CLIENTS." USING(ID_client) WHERE 1 ".$search_str." ".$order." LIMIT ".$num_start.",".$nb))
			{
				$datas['nb_results_liste'] = mysqli_num_rows($query);	
				while($datas['results'][] = mysqli_fetch_assoc($query))
				{
					
				}
				return($datas);
			}
			else
			{
				$datas['nb_results_liste'] = 0;
			}		
		
		}
		
	
	}
	else
	{
		$datas['nb_results_total'] = 0;
		$datas['nb_results_liste'] = 0;
	}
	return($datas);

}

function th_residences_nb_for_client($db, $ID_client)
{
	if($query = common_mysql_query($db, "SELECT COUNT(ID_residence) FROM ".MYSQL_TABLE_TH_RESIDENCES."  WHERE ID_client = '".$ID_client."' AND residence_status = 'ACTIF' ".$search_str))
	{
		$fetch_nb = mysqli_fetch_row($query);
		return($fetch_nb[0]);
	}	
	else
	{
		return(0);
	}
}

function th_residences_del($db, $ID_residence)
{
	//common_mysql_query($db, "DELETE FROM ".MYSQL_TABLE_TH_RESIDENCES." WHERE ID_residence = '".common_mysql_encode($db, $ID_residence)."'");
	common_mysql_query($db, "UPDATE ".MYSQL_TABLE_TH_RESIDENCES." SET residence_status = 'SUPPRIME' WHERE ID_residence = '".common_mysql_encode($db, $ID_residence)."'");
}

function th_residences_get_datas_by_nfcId($db, $nfcID)
{
	if($R1 = common_mysql_query($db, "SELECT * FROM ".MYSQL_TABLE_TH_RESIDENCES." WHERE residence_nfcId = '".common_mysql_encode($db, $nfcID)."'"))
	{
		$F1 = mysqli_fetch_assoc($R1);
		mysqli_free_result($R1);
		
		return($F1);
	}
	else
	{
		return(false);
	}
}


function th_residences_get_datas($db, $ID_residence)
{
	if($R1 = common_mysql_query($db, "SELECT * FROM ".MYSQL_TABLE_TH_RESIDENCES." WHERE ID_residence = '".common_mysql_encode($db, $ID_residence)."'"))
	{
		$F1 = mysqli_fetch_assoc($R1);
		mysqli_free_result($R1);
		
		return($F1);
	}
	else
	{
		return(false);
	}
}


function th_residences_update_field($db, $ID_residence , $field_name, $field_value)
{
	$R1 = common_mysql_query($db, "UPDATE ".MYSQL_TABLE_TH_RESIDENCES." SET ".$field_name." = '".common_mysql_encode($db, $field_value)."' WHERE ID_residence = '".common_mysql_encode($db, $ID_residence)."'");
}

function th_residences_create($db)
{
	common_mysql_query($db, 'INSERT INTO '.MYSQL_TABLE_TH_RESIDENCES." 
    					SET ID_residence = 0");

    return(mysqli_insert_id($db));
}

/*Fin th_residences (MYSQL_TABLE_TH_RESIDENCES)*/

/*Fonctions th_residences_contacts (MYSQL_TABLE_TH_RESIDENCES_CONTACTS)*/

function th_residences_contacts_raz($db, $ID_contact)
{
	common_mysql_query($db, "DELETE FROM ".MYSQL_TABLE_TH_RESIDENCES_CONTACTS." WHERE ID_contact = '".common_mysql_encode($db, $ID_contact)."'");
	
}

function th_residences_contacts_search( $db, $num_start='0', $nb='0', $tri='', $select_adds_fields='', $ID_residence='' )
{

	$search_str = " AND residence_status = 'ACTIF' AND contact_status = 'ACTIF'";
	
	if($ID_residence != '')
	{
		$search_str .= " AND ".MYSQL_TABLE_TH_RESIDENCES_CONTACTS.".ID_residence = '".common_mysql_encode($db, $ID_residence)."'";
	}

	switch($tri)
	{	
		default:
			$order = " ORDER BY contact_prenom, contact_nom";
		break;
	}
	
	//Nb total de resultats
	if($query = common_mysql_query($db, "SELECT COUNT(ID_rescon) FROM ".MYSQL_TABLE_TH_RESIDENCES_CONTACTS." INNER JOIN ".MYSQL_TABLE_TH_RESIDENCES." USING(ID_residence) INNER JOIN ".MYSQL_TABLE_TH_CLIENTS_CONTACTS." USING(ID_contact) WHERE 1 ".$search_str))
	{
		$nb_resultats_totaux = mysqli_fetch_row($query);
		mysqli_free_result($query);
		$datas['nb_results_total'] = $nb_resultats_totaux[0];	
		
		if($datas['nb_results_total'] > 0 && $nb > 0)
		{
			if($query = common_mysql_query($db, "SELECT DISTINCT(ID_rescon)".$select_adds_fields." FROM ".MYSQL_TABLE_TH_RESIDENCES_CONTACTS." INNER JOIN ".MYSQL_TABLE_TH_RESIDENCES." USING(ID_residence)  INNER JOIN ".MYSQL_TABLE_TH_CLIENTS_CONTACTS." USING(ID_contact) WHERE 1 ".$search_str." ".$order." LIMIT ".$num_start.",".$nb))
			{
				$datas['nb_results_liste'] = mysqli_num_rows($query);	
				while($datas['results'][] = mysqli_fetch_assoc($query))
				{
					
				}
				return($datas);
			}
			else
			{
				$datas['nb_results_liste'] = 0;
			}		
		
		}
		
	
	}
	else
	{
		$datas['nb_results_total'] = 0;
		$datas['nb_results_liste'] = 0;
	}
	return($datas);

}

function th_residences_nb_for_contact($db, $ID_contact)
{
	if($query = common_mysql_query($db, "SELECT COUNT(ID_rescon) FROM ".MYSQL_TABLE_TH_RESIDENCES_CONTACTS." INNER JOIN ".MYSQL_TABLE_TH_RESIDENCES." USING(ID_residence) WHERE ID_contact = '".$ID_contact."' AND residence_status = 'ACTIF' ".$search_str))
	{
		$fetch_nb = mysqli_fetch_row($query);
		return($fetch_nb[0]);
	}	
	else
	{
		return(0);
	}
}

function th_residences_contacts_del($db, $ID_rescon)
{
	common_mysql_query($db, "DELETE FROM ".MYSQL_TABLE_TH_RESIDENCES_CONTACTS." WHERE ID_rescon = '".common_mysql_encode($db, $ID_rescon)."'");
}

function th_residences_contacts_exists($db, $ID_residence, $ID_contact)
{
	if($R1 = common_mysql_query($db, "SELECT * FROM ".MYSQL_TABLE_TH_RESIDENCES_CONTACTS." WHERE ID_residence = '".common_mysql_encode($db, $ID_residence)."' AND ID_contact = '".$ID_contact."'"))
	{
		$F1 = mysqli_fetch_assoc($R1);
		mysqli_free_result($R1);
		
		return($F1);
	}
	else
	{
		return(false);
	}
}

function th_residences_contacts_get_datas($db, $ID_rescon)
{
	if($R1 = common_mysql_query($db, "SELECT * FROM ".MYSQL_TABLE_TH_RESIDENCES_CONTACTS." WHERE ID_rescon = '".common_mysql_encode($db, $ID_rescon)."'"))
	{
		$F1 = mysqli_fetch_assoc($R1);
		mysqli_free_result($R1);
		
		return($F1);
	}
	else
	{
		return(false);
	}
}


function th_residences_contacts_update_field($db, $ID_rescon , $field_name, $field_value)
{
	$R1 = common_mysql_query($db, "UPDATE ".MYSQL_TABLE_TH_RESIDENCES_CONTACTS." SET ".$field_name." = '".common_mysql_encode($db, $field_value)."' WHERE ID_rescon = '".common_mysql_encode($db, $ID_rescon)."'");
}

function th_residences_contacts_create($db)
{
	common_mysql_query($db, 'INSERT INTO '.MYSQL_TABLE_TH_RESIDENCES_CONTACTS." 
    					SET ID_rescon = 0");

    return(mysqli_insert_id($db));
}

/*Fin th_residences_contacts (MYSQL_TABLE_TH_RESIDENCES_CONTACTS)*/

	
?>