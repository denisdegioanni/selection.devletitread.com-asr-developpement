<?
/*****************
* SELECTION		 *
* Configuration  *
*****************/

date_default_timezone_set('Europe/Paris');

session_start();

/*site conf*/
define('CFG_SITE_NAMELONG', 'Selection - Intranet');
define('CFG_SITE_NAMESHORT', 'Selection');
define('CFG_SITE_TITLE', 'Selection - Intranet gestion');
define('CFG_SITE_MAIL', 'info@doingenia.com');//Email du site (affiche)

define('CFG_SITE_NBPARPAGE', 10);//Pour listes, nb affichage par page

define('CFG_SITE_DBUG_ML', 'info@doingenia.com');//Email pour envoi des messages de debug
define('CFG_DEBUG_IP', '0.0.0.0');//IP pour affichage messages erreur

//Affichage des erreurs si IP de debug
if($_SERVER['REMOTE_ADDR'] == CFG_DEBUG_IP)
{
	ini_set('display_errors',1);
	error_reporting(E_ERROR | E_WARNING | E_PARSE);
}

/*google analytics*/
define('CFG_SITE_ANALYTICS_ID', '');//ID analytics du site

define('CFG_PATH_UPLOADS', dirname(__FILE__).'/../uploaded_datas/');
define('CFG_PATH_UPLOADS_RELATIVE', '/uploaded_datas/');

define('CFG_DATE_SHORT_FORMAT', 'd/m/Y');
define('CFG_DATE_LONG_FORMAT', 'd/m/Y H:i');

/*info envoi alertes non passage*/
define('CFG_PASSAGE_ALERTE_MAIL_EXPED', 'info@doingenia.com');
define('CFG_PASSAGE_ALERTE_NAME_EXPED', 'Do Ingenia');
$CFG_PASSAGE_ALERTE_DESTINATAIRES = array('info@doingenia.com');

/*mySQL*/
define('MYSQL_HOST', '46.255.163.201');
define('MYSQL_USER', 'p1300_17');
define('MYSQL_PWD', '4AUEI2KGEyQc');
define('MYSQL_DB', 'p1300_17');

//Liste des tables SQL
define('MYSQL_TABLE_INTERVENANTS', 'th_intervenants');
define('MYSQL_TABLE_CFG_ADMIN_RIGHTS', 'th_cfg_admin_rights');
define('MYSQL_TABLE_CFG_ADMIN_ROLES', 'th_cfg_admin_roles');

define('MYSQL_TABLE_TH_CLIENTS', 'th_clients');
define('MYSQL_TABLE_TH_CLIENTS_CONTACTS', 'th_clients_contacts');

define('MYSQL_TABLE_TH_RESIDENCES', 'th_residences');
define('MYSQL_TABLE_TH_RESIDENCES_CONTACTS', 'th_residences_contacts');

//Niveau d'un utilisateur accedant a l'espace client (pour gestion droits)
define('CFG_ID_ADMROLE_CLIENT', 2);

define('CFG_PASSPLANING_YEAR_START', 2015);//Annee de demarrage pour les panneaux pass planing
define('CFG_PASSPLANING_NB_INTERVAL_WEEKS_MAX', 4);//Nb semaines maxi intervalle

//Valeurs ENUM
$MYSQL_ENUM_DATA_CLIENT_TYPE = array(0 => "Non défini", 1 => "Type #1", 2 => "Type #2", 3 => "Type #3", 9 => "Autre");//
$MYSQL_ENUM_DATA_CLIENT_PAYS = array(1 => "France", 2 => "Monaco", 3 => "Italie", 9 => "Autre");
$MYSQL_ENUM_DATA_CLIENT_ZONE = array(1 => "1", 2 => "2", 3 => "3", 4 => "4", 5 => "5", 6 => "6", 7 => "7");
$MYSQL_ENUM_DATA_RESIDENCE_PAYS = array(1 => "France", 2 => "Monaco", 3 => "Italie", 9 => "Autre");

$MYSQL_ENUM_DATA_CONTACT_FONCTION = array(1 => "fonction #1", 2 => "fonction #2", 3 => "fonction #3", 9 => "Autre");

$MYSQL_ENUM_DATA_TECHNICIEN_TYPE = array(0 => "Non défini", 1 => "Agent de maintenance", 2 => "Special", 3 => "Autre");
$MYSQL_ENUM_DATA_TECHNICIEN_PROFIL = array(0 => "Profil #1", 1 => "Profil #2", 2 => "Profil #3", 3 => "Profil #4", 4 => "Profil #5");

$MYSQL_ENUM_DATA_TECHNICIEN_ACTIF = array(1 => "Actif", 2 => "Inactif");

?>