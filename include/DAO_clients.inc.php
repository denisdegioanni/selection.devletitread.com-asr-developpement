<?php
/*
	FONCTION D'ACCES AUX DONNEES Tables th_clients + th_clients_contacts
*/	
	
/*Fonctions th_clients (MYSQL_TABLE_TH_CLIENTS)*/

function th_clients_search( $db, $num_start='0', $nb='0', $tri='', $select_adds_fields='' )
{

	$search_str = " AND client_status = 'ACTIF' ";

	switch($tri)
	{	
		default:
			$order = "";
		break;
	}
	
	//Nb total de resultats
	if($query = common_mysql_query($db, "SELECT COUNT(ID_client) FROM ".MYSQL_TABLE_TH_CLIENTS." WHERE 1 ".$search_str))
	{
		$nb_resultats_totaux = mysqli_fetch_row($query);
		mysqli_free_result($query);
		$datas['nb_results_total'] = $nb_resultats_totaux[0];	
		
		if($datas['nb_results_total'] > 0 && $nb > 0)
		{
			if($query = common_mysql_query($db, "SELECT DISTINCT(ID_client)".$select_adds_fields." FROM ".MYSQL_TABLE_TH_CLIENTS." WHERE 1 ".$search_str." ".$order." LIMIT ".$num_start.",".$nb))
			{
				$datas['nb_results_liste'] = mysqli_num_rows($query);	
				while($datas['results'][] = mysqli_fetch_assoc($query))
				{
					
				}
				return($datas);
			}
			else
			{
				$datas['nb_results_liste'] = 0;
			}		
		
		}
		
	
	}
	else
	{
		$datas['nb_results_total'] = 0;
		$datas['nb_results_liste'] = 0;
	}
	return($datas);

}

function th_clients_del($db, $ID_client)
{
	//common_mysql_query($db, "DELETE FROM ".MYSQL_TABLE_TH_CLIENTS." WHERE ID_client = '".common_mysql_encode($db, $ID_client)."'");
	common_mysql_query($db, "UPDATE ".MYSQL_TABLE_TH_CLIENTS." SET client_status = 'SUPPRIME' WHERE ID_client = '".common_mysql_encode($db, $ID_client)."'");
}


function th_clients_get_datas($db, $ID_client)
{
	if($R1 = common_mysql_query($db, "SELECT * FROM ".MYSQL_TABLE_TH_CLIENTS." WHERE ID_client = '".common_mysql_encode($db, $ID_client)."'"))
	{
		$F1 = mysqli_fetch_assoc($R1);
		mysqli_free_result($R1);
		
		return($F1);
	}
	else
	{
		return(false);
	}
}

function th_clients_exists_email($db, $login)
{
	if($R1 = common_mysql_query($db, "SELECT ID_client FROM ".MYSQL_TABLE_TH_CLIENTS." WHERE client_email = '".common_mysql_encode($db, strtolower($login))."'"))
	{
		$F1 = mysqli_fetch_assoc($R1);
		mysqli_free_result($R1);
		return($F1['ID_client']);
	}
	else
	{
		return(false);
	}
}


function th_clients_update_field($db, $ID_client , $field_name, $field_value)
{
	if($field_name == 'client_email')
	{
		$field_value = strtolower($field_value);
	}
	
	$R1 = common_mysql_query($db, "UPDATE ".MYSQL_TABLE_TH_CLIENTS." SET ".$field_name." = '".common_mysql_encode($db, $field_value)."' WHERE ID_client = '".common_mysql_encode($db, $ID_client)."'");
}

function th_clients_create($db)
{
	common_mysql_query($db, 'INSERT INTO '.MYSQL_TABLE_TH_CLIENTS." 
    					SET ID_client = 0");

    return(mysqli_insert_id($db));
}

/*Fin th_clients (MYSQL_TABLE_TH_CLIENTS)*/

/*Fonctions th_clients_contacts (MYSQL_TABLE_TH_CLIENTS_CONTACTS)*/

function th_clients_contacts_search( $db, $num_start='0', $nb='0', $tri='', $select_adds_fields='', $ID_client='' )
{
	$search_str = " AND client_status = 'ACTIF' AND contact_status = 'ACTIF'";
	
	if($ID_client != '')
	{
		$search_str .= " AND ID_client = '".$ID_client."'";		
	}

	switch($tri)
	{	
		default:
			$order = " ORDER BY contact_nom, contact_prenom";
		break;
	}
	
	//Nb total de resultats
	if($query = common_mysql_query($db, "SELECT COUNT(ID_contact) FROM ".MYSQL_TABLE_TH_CLIENTS_CONTACTS." INNER JOIN ".MYSQL_TABLE_TH_CLIENTS." USING(ID_client) WHERE 1 ".$search_str))
	{
		$nb_resultats_totaux = mysqli_fetch_row($query);
		mysqli_free_result($query);
		$datas['nb_results_total'] = $nb_resultats_totaux[0];	
		
		if($datas['nb_results_total'] > 0 && $nb > 0)
		{
			if($query = common_mysql_query($db, "SELECT DISTINCT(ID_contact)".$select_adds_fields." FROM ".MYSQL_TABLE_TH_CLIENTS_CONTACTS." INNER JOIN ".MYSQL_TABLE_TH_CLIENTS." USING(ID_client) WHERE 1 ".$search_str." ".$order." LIMIT ".$num_start.",".$nb))
			{
				$datas['nb_results_liste'] = mysqli_num_rows($query);	
				while($datas['results'][] = mysqli_fetch_assoc($query))
				{
					
				}
				return($datas);
			}
			else
			{
				$datas['nb_results_liste'] = 0;
			}		
		
		}
		
	
	}
	else
	{
		$datas['nb_results_total'] = 0;
		$datas['nb_results_liste'] = 0;
	}
	return($datas);

}

function th_clients_contacts_del($db, $ID_contact)
{
	//common_mysql_query($db, "DELETE FROM ".MYSQL_TABLE_TH_CLIENTS_CONTACTS." WHERE ID_contact = '".common_mysql_encode($db, $ID_contact)."'");
	common_mysql_query($db, "UPDATE ".MYSQL_TABLE_TH_CLIENTS_CONTACTS." SET contact_status = 'SUPPRIME' WHERE ID_contact = '".common_mysql_encode($db, $ID_contact)."'");	
}


function th_clients_contacts_get_datas($db, $ID_contact)
{
	if($R1 = common_mysql_query($db, "SELECT * FROM ".MYSQL_TABLE_TH_CLIENTS_CONTACTS." WHERE ID_contact = '".common_mysql_encode($db, $ID_contact)."'"))
	{
		$F1 = mysqli_fetch_assoc($R1);
		mysqli_free_result($R1);
		
		return($F1);
	}
	else
	{
		return(false);
	}
}


function th_clients_contacts_update_field($db, $ID_contact , $field_name, $field_value)
{
	$R1 = common_mysql_query($db, "UPDATE ".MYSQL_TABLE_TH_CLIENTS_CONTACTS." SET ".$field_name." = '".common_mysql_encode($db, $field_value)."' WHERE ID_contact = '".common_mysql_encode($db, $ID_contact)."'");
}

function th_clients_contacts_create($db)
{
	common_mysql_query($db, 'INSERT INTO '.MYSQL_TABLE_TH_CLIENTS_CONTACTS." 
    					SET ID_contact = 0");

    return(mysqli_insert_id($db));
}

/*Fin th_clients_contacts (MYSQL_TABLE_TH_CLIENTS_CONTACTS)*/

?>