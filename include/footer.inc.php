				</section>
			</section>
		</section>
		<!-- Placed js at the end of the document so the pages load faster -->
		
		<!--Core js-->
		
		<script type="text/javascript" src="/js/fonctions.js"></script>	    		
		<script src="theme/BucketAdmin/html/bs3/js/bootstrap.min.js"></script>
		<script class="include" type="text/javascript" src="theme/BucketAdmin/html/js/jquery.dcjqaccordion.2.7.js"></script>
		<script src="theme/BucketAdmin/html/js/jquery.scrollTo.min.js"></script>
		<script src="theme/BucketAdmin/html/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
		<script src="theme/BucketAdmin/html/js/jquery.nicescroll.js"></script>

		<!--datepicker-->		
		<script type="text/javascript" src="theme/BucketAdmin/html/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="theme/BucketAdmin/html/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
		<script type="text/javascript" src="theme/BucketAdmin/html/js/bootstrap-daterangepicker/moment.min.js"></script>
		<script type="text/javascript" src="theme/BucketAdmin/html/js/bootstrap-daterangepicker/daterangepicker.js"></script>
		
		<!--common script init for all pages-->
		<script src="theme/BucketAdmin/html/js/scripts.js"></script>	
		<!--<script src="theme/BucketAdmin/html/js/advanced-form.js"></script>			-->
		
		<!--dynamic table-->
		<script type="text/javascript" language="javascript" src="theme/BucketAdmin/html/js/advanced-datatable/js/jquery.dataTables.js"></script>
		<script type="text/javascript" src="theme/BucketAdmin/html/js/data-tables/DT_bootstrap.js"></script>    
		<script src="theme/BucketAdmin/html/js/dynamic_table_init.js"></script>		
		
		

		<?
		//Fenetre edition
		//require(dirname(__FILE__).'/modales/travail-edition-fiche.inc.php');
		if(ident_right_check(1))//Accès gestion liste clients + residences
		{
			require(dirname(__FILE__).'/modales/client-edition-fiche.inc.php');						
			require(dirname(__FILE__).'/modales/residence-edition-fiche.inc.php');			
		}
	
		require(dirname(__FILE__).'/modales/alerte-suppression.inc.php');
		

		if(CFG_SITE_ANALYTICS_ID != '')
		{
			//Code Google Analytics
			?>
			<script type="text/javascript">
			</script>			
			<?
		}
		?>		
	</body>
</html>
