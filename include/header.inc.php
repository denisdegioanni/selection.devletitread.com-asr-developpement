<?
if(!is_array($_SESSION['LOGIN']))
{
	header('Location:/?err=1');
	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="/img/favicon.png">
    <title><? print CFG_SITE_TITLE;?></title>

    <!--Core CSS -->
    <link href="theme/BucketAdmin/html/bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="theme/BucketAdmin/html/css/bootstrap-reset.css" rel="stylesheet">
    <link href="theme/BucketAdmin/html/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="theme/BucketAdmin/html/css/style.css" rel="stylesheet">
    <link href="theme/BucketAdmin/html/css/style-responsive.css" rel="stylesheet" />
    
	<link rel="stylesheet" type="text/css" href="theme/BucketAdmin/html/js/bootstrap-datepicker/css/datepicker.css">    
	<link rel="stylesheet" type="text/css" href="theme/BucketAdmin/html/js/bootstrap-daterangepicker/daterangepicker-bs3.css">
	<link rel="stylesheet" type="text/css" href="theme/BucketAdmin/html/js/bootstrap-datetimepicker/css/datetimepicker.css">
    
    <!--dynamic table-->
    <link href="theme/BucketAdmin/html/js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
    <link href="theme/BucketAdmin/html/js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
    <link rel="stylesheet" href="theme/BucketAdmin/html/js/data-tables/DT_bootstrap.css" />   

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="theme/BucketAdmin/html/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
	<link rel="stylesheet" type="text/css" href="css/style.css">   
	
	<script src="theme/BucketAdmin/html/js/jquery.js"></script>    
	<script src="js/fileuploader.js"></script>    
    
</head>
<body>
<section id="container" >
<!--header start-->
<header class="header fixed-top clearfix">
<!--logo start-->
<div class="brand">

    <a href="#nolink" class="logo">
    	<!--<img src="img/logo.png"/>-->
    	Selection
    	<br/>
    	<small>Intranet de gestion</small>
    </a>
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars"></div>
    </div>
</div>

<div class="top-nav clearfix">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
    	<!--
        <li>
            <input type="text" class="form-control search" placeholder="Rechercher">
        </li>
        -->
        <!-- user login dropdown start-->
       	<?
	    switch($_SESSION['LOGIN']['type'])
	    {
		    case 'admin':
				if($cur_intervenant_datas = intervenants_get_datas($db, $_SESSION['LOGIN']['ID_intervenant']))
				{
				   ?>
					<li class="dropdown">
				        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
				            <img alt="" src="theme/BucketAdmin/html/images/avatar1_small.jpg">
				            <span class="username"><? print str_txt_format($cur_intervenant_datas['intervenant_prenom']).' '.str_txt_format($cur_intervenant_datas['intervenant_nom']);?></span>
				            <b class="caret"></b>
				        </a>
				        <ul class="dropdown-menu extended logout">
				            <li><a href="/?err=4"><i class="fa fa-key"></i> Déconnexion</a></li>
				        </ul>	        
					</li>
				   <?	
				}		    
		    break;
		    
		    case 'client':
				if($cur_intervenant_datas = th_clients_get_datas($db, $_SESSION['LOGIN']['ID_intervenant']))
				{
				   ?>
					<li class="dropdown">
				        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
				            <img alt="" src="theme/BucketAdmin/html/images/avatar1_small.jpg">
				            <span class="username"><? print str_txt_format($cur_intervenant_datas['client_rs']);?></span>
				            <b class="caret"></b>
				        </a>
				        <ul class="dropdown-menu extended logout">
				            <li><a href="/?err=4"><i class="fa fa-key"></i> Déconnexion</a></li>
				        </ul>	        
					</li>
				   <?	
				}		    
		    break;
	    }

       	?>
        <!-- user login dropdown end -->
    </ul>
    <!--search & user info end-->
</div>
</header>
<!--header end-->

<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->            
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
	        <?
		    if(  ident_right_check(1) )//Gestion clients
		    {
			?>
	            <li>
	                <a href="#null" <? if($_SERVER['PHP_SELF'] == '/clients-liste.php') { print 'class="active"'; } ?>>
	                    <i class="fa fa-briefcase"></i>
	                    <span>Gestion clients</span>
	                </a>
	                <ul class="sub">
		                <li><a href="clients-liste.php">Liste des clients</a></li>	
	                </ul>                
	            </li>			
			<?			    
	        }
            ?>            
            <li>
                <a href="/?err=4">
                    <i class="fa fa-sign-out"></i>
                    <span>Déconnexion</span>
                </a>
            </li>
        </ul></div>        
<!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
        <!-- page start-->
