<?
require("include/config.inc.php");
require("include/fonctions.inc.php");
$db = common_mysql_connect(MYSQL_DB);

ident_page_protect(1);//Gestion clients


	/*
	 * Script:    DataTables server-side script for PHP and MySQL
	 * Copyright: 2010 - Allan Jardine
	 * License:   GPL v2 or BSD (3-point)
	 */
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * Easy set variables
	 */
	
	/* Array of database columns which should be read and sent back to DataTables. Use a space where
	 * you want to insert a non-database field (for example a counter or static image)
	 */
	 
	/*
	DEBUT ZONE EDITABLE 1 : Liste des champs de base des colonnes + champs additionnels si besoin		
	*/
	$aColumns = array( 'client_rs', 'client_depuis', 'client_type', 'client_ville' );
	$additionalColums = ' , ID_client';//As t on besoin d'autres infos qui ne seront pas affichées ?


	/* Indexed column (used for fast and accurate table cardinality) */
	$sIndexColumn = "ID_client";
	
	/* DB table to use */
	$sTable = MYSQL_TABLE_TH_CLIENTS;
	
	//Requete annexes (filtres autres)
	$search_str = ' ';
	$jointure = '';
	
	//Fin requetes annexes	

	/*
	FIN ZONE EDITABLE 1 : Voir ci-dessous pour zone editable 2 (formattage colonne)
	*/	
	
	
	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * If you just want to use the basic configuration for DataTables with PHP server-side, there is
	 * no need to edit below this line
	 */
	
	
	/* 
	 * Paging
	 */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' )
	{
		$sLimit = "LIMIT ".mysqli_real_escape_string( $db, $_GET['iDisplayStart'] ).", ".
			mysqli_real_escape_string( $db, $_GET['iDisplayLength'] );
	}
	
	
	/*
	 * Ordering
	 */
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ )
		{
			if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" )
			{
				$sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]."
				 	".mysqli_real_escape_string( $db, $_GET['sSortDir_'.$i] ) .", ";
			}
		}
		
		$sOrder = substr_replace( $sOrder, "", -2 );
		if ( $sOrder == "ORDER BY" )
		{
			$sOrder = "";
		}
	}
	
	
	/* 
	 * Filtering
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "";
	if ( $_GET['sSearch'] != "" )
	{
		$sWhere = "WHERE (";
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			$sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string( $db, $_GET['sSearch'] )."%' OR ";
		}
		$sWhere = substr_replace( $sWhere, "", -3 );
		$sWhere .= ')';
	}
	
	/* Individual column filtering */
	for ( $i=0 ; $i<count($aColumns) ; $i++ )
	{
		if ( $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' )
		{
			if ( $sWhere == "" )
			{
				$sWhere = "WHERE ";
			}
			else
			{
				$sWhere .= " AND ";
			}
			$sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($db, $_GET['sSearch_'.$i])."%' ";
		}
	}
	
	
	/*
	 * SQL queries
	 * Get data to display
	 */
	if($sWhere == '')
	{
		$search_str_tmp = ' WHERE 1 '.$search_str;
	}
	else
	{
		$search_str_tmp = $search_str;
	}
	
	
	$sQuery = "
		SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))." $additionalColums
		FROM   $sTable $jointure
		$sWhere
		$search_str_tmp
		$sOrder
		$sLimit
	";
	$rResult = mysqli_query( $db, $sQuery ) or die(mysqli_error($db));

	
	/* Data set length after filtering */
	$sQuery = "
		SELECT FOUND_ROWS()
	";
	$rResultFilterTotal = mysqli_query( $db, $sQuery ) or die(mysqli_error($db));
	$aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];
	
	/* Total data set length */
	$sQuery = "
		SELECT COUNT(".$sIndexColumn.")
		FROM   $sTable $jointure WHERE 1 $search_str
	";
	$rResultTotal = mysqli_query( $db, $sQuery ) or die(mysqli_error($db));
	$aResultTotal = mysqli_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];
	
	
	/*
	 * Output
	 */
	$output = array(
		"sEcho" => intval($_GET['sEcho']),
		"iTotalRecords" => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData" => array()
	);
	
	while ( $aRow = mysqli_fetch_assoc( $rResult ) )
	{
		$row = array();
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			/*
			DEBUT ZONE EDITABLE 2 : Formattage colonnes
			*/

			
			//Version par defaut
			/*
			if ( $aColumns[$i] == "version" )
			{
				//Special output formatting for 'version' column 
				$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
			}
			else if ( $aColumns[$i] != ' ' )
			{
				// General output
				$row[] = $aRow[ $aColumns[$i] ];
			}				
			*/
			//Formattage specifique a cette page
			
			switch($aColumns[$i])
			{
				case 'client_rs':
				
					$row[$i] = '<a href="clients-detail.php?id='.$aRow['ID_client'].'">'.str_truncate(str_txt_format($aRow['client_rs']), 200).'</a>';
					
				break;
				case 'client_type':
				
					$row[$i] = $MYSQL_ENUM_DATA_CLIENT_TYPE[$aRow['client_type']];
					
				break;				
				case 'client_depuis':
					
					//Affichage lien modif statut
					$row[$i] = date(CFG_DATE_SHORT_FORMAT, strtotime($aRow['client_depuis']));	
					
				break;	
				default:
					
					$row[$i] = '<a href="#edit" data-toggle="modal" data-target="#clientEditModal" onClick="clientEdit(\''.$aRow['ID_client'].'\');">'.str_truncate(str_txt_format($aRow['client_ville']), 60).'</a>';						

				break;
			}
		}
		
		//Colonne additionnelle avec le nb de residences
		$row[$i] = '<a href="clients-detail.php?id='.$aRow['ID_client'].'">'.th_residences_nb_for_client($db, $aRow['ID_client']).'</a>';			
		$i++;
		
		//Si on a une colonne additionnelle avec les actions, c'est a traiter ici		
		$row[$i] = '<a href="clients-detail.php?id='.$aRow['ID_client'].'" onClick="clientEdit(\''.$aRow['ID_client'].'\');">								    
    					fiche
    				</a>	
    				|									
    				<a href="#edit" data-toggle="modal" data-target="#clientEditModal"  onClick="clientEdit(\''.$aRow['ID_client'].'\');">								    
    					modifier
    				</a>	
    				|									
    				<a href="#suppr" data-toggle="modal" data-target="#confirmDeleteModal"  onClick="clientDeleteConfirm(\''.$aRow['ID_client'].'\');">								    
    					supprimer
    				</a>';	
		

		/*
		FIN ZONE EDITABLE 2 : Formattage colonnes
		*/
		
		$output['aaData'][] = $row;
		
	}
	
	//print_r($output);
	
	echo json_encode( $output );
	?>