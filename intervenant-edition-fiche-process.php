<?
require_once(dirname(__FILE__).'/include/config.inc.php');
require_once(dirname(__FILE__).'/include/fonctions.inc.php');

$db = common_mysql_connect(MYSQL_DB);

ident_page_protect(9);//Gestion admin

/*
//Affichage liste champs pour recup
foreach($_POST as $field_name => $value)
{
	print '$'.$field_name.' = $_POST[\''.$field_name.'\'];<br/>';
}
foreach($_POST as $field_name => $value)
{
	print '$'.$field_name.' = $_POST[\''.$field_name.'\'];'."\n";
}
exit();
*/

$errfield 		= $_POST['errfield'];
$ID_intervenant	= $_POST['ID_intervenant'];
$ID_admrole		= $_POST['ID_admrole'];
$nom 			= $_POST['nom'];
$prenom 		= $_POST['prenom'];
$login	 		= $_POST['upd_login'];
$pwd	 		= $_POST['upd_pwd'];
$email	 		= $_POST['email'];
$equipe	 		= $_POST['equipe'];
$type			= $_POST['type'];
if($_POST['ID_intervenants_associes'])
{
	$ID_intervenants_associes = implode(',', $_POST['ID_intervenants_associes']);	
}
else
{
	$ID_intervenants_associes = 0;
}


//Verif champs obligatoires
$mandatory_fields = array('ID_intervenant', 'nom', 'ID_admrole');

foreach($mandatory_fields as $null => $mand_field_name)
{
	$_POST[$mand_field_name] = trim($_POST[$mand_field_name]);
	if($_POST[$mand_field_name] == '')
	{
		//Champ obligatoire non rempli (teste au niveau JS normalement donc on renvoie une erreur sommaire)
		?>
		$('#<? print $errfield;?>').html("<div class=\"alert alert-danger\">Un champ obligatoire (<? print $mand_field_name;?>) n'a pas été correctement rempli. Merci de vérifier le formulaire.</div>");
		<?
		exit();
	}
}

if($ID_intervenant == 0)
{
	//Creation nouvel enregistrement
	$ID_intervenant = intervenants_create($db);
	$creation = 1;
	
}
else
{
	$creation = 0;
	if($intervenant_datas = intervenants_get_datas($db, $ID_intervenant))
	{
	}
	else
	{
		?>
		$('#<? print $errfield;?>').html("<div class=\"alert alert-danger\">Impossible d'editer la fiche <? print $ID_intervenant;?>...Erreur technique...</div>");
		<?
		exit();		
	}
	
}

//Mise a jour des champs
intervenants_update_field($db,$ID_intervenant, 'ID_admrole', $ID_admrole);
intervenants_update_field($db,$ID_intervenant, 'intervenant_nom', $nom);
intervenants_update_field($db,$ID_intervenant, 'intervenant_prenom', $prenom);
intervenants_update_field($db,$ID_intervenant, 'intervenant_login', $login);
intervenants_update_field($db,$ID_intervenant, 'intervenant_nom', $nom);
intervenants_update_field($db,$ID_intervenant, 'intervenant_pwd', $pwd);
intervenants_update_field($db,$ID_intervenant, 'intervenant_email', $email);

//Tout est ok, on ferme la modale
if($creation == 1)
{
?>
	window.location='intervenants-liste.php?confirm=1';
<?
}
else
{
?>
	$('#intervenantEditModal').modal('hide');
	window.location.reload();
<?
}
?>