<?
require("include/config.inc.php");
require("include/fonctions.inc.php");
unset($_SESSION['LOGIN']);
$err = $_GET['err'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="/img/favicon.png">

    <title>Intranet - Connexion</title>

    <!--Core CSS -->
    <link href="theme/BucketAdmin/html/bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="theme/BucketAdmin/html/css/bootstrap-reset.css" rel="stylesheet">
    <link href="theme/BucketAdmin/html/font-awesome/css/font-awesome.css" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="theme/BucketAdmin/html/css/style.css" rel="stylesheet">
    <link href="theme/BucketAdmin/html/css/style-responsive.css" rel="stylesheet" />
    
    <script src="theme/BucketAdmin/html/js/jquery.js"></script>    
    <script src="js/fonctions.js"></script>    

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

  <body class="login-body">

    <div class="container">

		<div class="login-logo" align="center"><img src="/img/client_type_1.jpg"></div>
		
		<form action="login-check.php" class="form-signin" type="post" id="loginForm" submit-checkfields="1" submit-ajax="1" submit-checkfields-error="loginFormErrField">
		  <input type="hidden" name="errfield" value="loginFormErrField" class="form-control">
		  <h2 class="form-signin-heading">identification</h2>
		  <div class="login-wrap">
		      <div class="user-login-info">
		          <input type="text" name="login" mandatory_type="string" mandatory_value="2" mandatory_fielderr="Merci de saisir un identifiant valide"  class="form-control" placeholder="Login" autofocus>
		          <input type="password" name="mdp" mandatory_type="string" mandatory_value="2" mandatory_fielderr="Merci de saisir un mot de passe valide"   class="form-control" placeholder="Mot de passe">
		      </div>
		      
			  <div id="loginFormErrField">
			      <?
			      switch($err)
			      {
			      	case '1':
			      		print '<div class="alert alert-danger">Identification incorrecte. Merci de vérifier vos accès.</div>';
			      	break;
			      	case '2':
			      		print  '<div class="alert alert-danger">Vous devez être identifié pour accéder à cette fonction.</div>';
			      	break;	
			      	case '3':
			      		print '<div class="alert alert-danger">Vous n\'avez pas les droits d\'accès à cette fonction.</div>';
			      	break;	
			      	case '4':
			      		print '<div class="alert alert-danger">Vous avez bien été déconnecté.</div>';
			      	break;		
			      }
			      ?>								
			  </div>		      
		      
		      <input type="submit" value="M'identifier" class="btn btn-lg btn-login btn-block" data-loading-text="Chargement...">
		  </div>
		</form>

    </div>

    <!-- Placed js at the end of the document so the pages load faster -->

    <!--Core js-->
    <script src="theme/BucketAdmin/html/bs3/js/bootstrap.min.js"></script>
    
	<!--script for this page-->
	<?
	if(CFG_SITE_ANALYTICS_ID != '')
	{
		//Code Google Analytics
		?>
		<script type="text/javascript">
	
			  var _gaq = _gaq || [];
			  _gaq.push(['_setAccount', '<? print addslashes(CFG_SITE_ANALYTICS_ID);?>']);
			  _gaq.push(['_setDomainName', '<? print $_SERVER['HTTP_HOST']?>']);
			  _gaq.push(['_trackPageview']);
			
			  (function() {
			    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			  })();
			
		</script>			
		<?
	}
	?>    

  </body>
</html>
