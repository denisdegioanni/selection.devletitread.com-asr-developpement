<?
require_once(dirname(__FILE__).'/include/config.inc.php');
require_once(dirname(__FILE__).'/include/fonctions.inc.php');

$db = common_mysql_connect(MYSQL_DB);

ident_page_protect(1);//Gestion clients


//Affichage liste champs pour recup
/*
foreach($_POST as $field_name => $value)
{
	print '$'.$field_name.' = $_POST[\''.$field_name.'\'];<br/>';
}
foreach($_POST as $field_name => $value)
{
	print '$'.$field_name.' = $_POST[\''.$field_name.'\'];'."\n";
}
exit();
*/

$errfield 	= $_POST['errfield'];
$ID_client 	= $_POST['ID_client'];
$email 		= $_POST['email'];
$upd_pwd 	= $_POST['upd_pwd'];
$type 		= $_POST['type'];
$date_depuis = common_fr_date_sql($_POST['date_depuis']);
$rs 		= strtoupper(common_remove_accents($_POST['rs']));
$adresse 	= $_POST['adresse'];
$adresse_suite = $_POST['adresse_suite'];
$cpost 		= $_POST['cpost'];
$ville 		= strtoupper(common_remove_accents($_POST['ville']));
$pays 		= $_POST['pays'];
$email_dedie = $_POST['email_dedie'];
$zone		= $_POST['zone'];


//Verif champs obligatoires
$mandatory_fields = array('ID_client', 'email', 'rs', 'ville', 'cpost', 'pays');

foreach($mandatory_fields as $null => $mand_field_name)
{
	$_POST[$mand_field_name] = trim($_POST[$mand_field_name]);
	if($_POST[$mand_field_name] == '')
	{
		//Champ obligatoire non rempli (teste au niveau JS normalement donc on renvoie une erreur sommaire)
		?>
		$('#<? print $errfield;?>').html("<div class=\"alert alert-danger\">Un champ obligatoire (<? print $mand_field_name;?>) n'a pas été correctement rempli. Merci de vérifier le formulaire.</div>");
		<?
		exit();
	}
}

if($ID_client == 0)
{
	//Creation nouvel enregistrement
	$ID_client = th_clients_create($db);
	$creation = 1;
	
}
else
{
	$creation = 0;
	if($client_datas = th_clients_get_datas($db, $ID_client))
	{
	}
	else
	{
		?>
		$('#<? print $errfield;?>').html("<div class=\"alert alert-danger\">Impossible d'editer la fiche <? print $ID_client;?>...Erreur technique...</div>");
		<?
		exit();		
	}
	
}

//Mise a jour des champs
if($upd_pwd != '')
{
	th_clients_update_field($db, $ID_client, 'client_password', $upd_pwd);	
}
th_clients_update_field($db, $ID_client, 'client_email', $email);
th_clients_update_field($db, $ID_client, 'client_type', $type);
th_clients_update_field($db, $ID_client, 'client_depuis', $date_depuis);
th_clients_update_field($db, $ID_client, 'client_rs', $rs);
th_clients_update_field($db, $ID_client, 'client_adresse', $adresse);
th_clients_update_field($db, $ID_client, 'client_adresse_suite', $adresse_suite);
th_clients_update_field($db, $ID_client, 'client_cpost', $cpost);
th_clients_update_field($db, $ID_client, 'client_ville', $ville);
th_clients_update_field($db, $ID_client, 'client_pays', $adresse_pays);
th_clients_update_field($db, $ID_client, 'client_email_dedie', $email_dedie);
th_clients_update_field($db, $ID_client, 'client_zone', $zone);

//Tout est ok, on ferme la modale
if($creation == 1)
{
?>
	window.location='clients-liste.php?confirm=1';
<?
}
else
{
?>
	$('#clientEditModal').modal('hide');
	window.location.reload();
<?
}
?>