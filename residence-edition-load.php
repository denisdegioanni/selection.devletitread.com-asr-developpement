<?
require_once(dirname(__FILE__).'/include/config.inc.php');
require_once(dirname(__FILE__).'/include/fonctions.inc.php');

$db = common_mysql_connect(MYSQL_DB);

ident_page_protect(1);//Gestion residences

$ID_residence 	= $_REQUEST['id'];
$ID_client 		= $_REQUEST['client'];

if($ID_residence == '0' && ($ID_client == '0' || $ID_client == 'undefined' || !is_numeric($ID_client)))//Pas normal
{
	?>
	alert("Aucun client sélectionné, nous ne pouvons créer une résidence, merci de raffraichir la page et de réessayer. Si le problème persiste, merci le signaler à l'équipe de développement.");
	<?
}
elseif($ID_residence == '0')
{
	?>
	$('#residenceEditForm input[name="ID_residence"]').val("0");	
	$('#residenceEditForm input[name="ID_client"]').val("<? print $ID_client;?>");	
	$('#residenceEditForm input[type=text],#residenceEditForm input[type=date],#residenceEditForm input[type=number]').val('');	
	$('#residenceEditFormLabel').html('Nouvelle résidence');
	<?
}
elseif($residence_datas = th_residences_get_datas($db, $ID_residence))//Verification droit
{
   	?>
   	if($('#residenceEditForm').length> 0)
   	{
   		$('#residenceEditForm input[name="ID_residence"]').val("<? print $ID_residence;?>");
   		$('#residenceEditForm input[name="ID_client"]').val("0");	   		
   		$('#residenceEditForm input[name="batiment"]').val('<? print addslashes($residence_datas['residence_batiment']);?>');
   		$('#residenceEditForm input[name="entree"]').val('<? print addslashes($residence_datas['residence_entree']);?>');
   		$('#residenceEditForm input[name="nom"]').val('<? print addslashes($residence_datas['residence_nom']);?>');
   		$('#residenceEditForm input[name="adresse"]').val('<? print addslashes($residence_datas['residence_adresse']);?>');
   		$('#residenceEditForm input[name="adresse_suite"]').val('<? print addslashes($residence_datas['residence_adresse_suite']);?>');
   		$('#residenceEditForm input[name="cpost"]').val('<? print addslashes($residence_datas['residence_cpost']);?>');
   		$('#residenceEditForm input[name="ville"]').val('<? print addslashes($residence_datas['residence_ville']);?>');
   		$('#residenceEditForm input[name="pays"]').val('<? print addslashes($residence_datas['residence_pays']);?>');
   		$('#residenceEditForm input[name="nfcId"]').val('<? print addslashes($residence_datas['residence_nfcId']);?>');
   		
   		$('#residenceEditFormLabel').html('<? print addslashes($residence_datas['residence_nom']);?>');
   	}
   	<?		
}
else
{
	?>
	alert("Fiche inexistante (ne peut être modifiée).");
	<?
	exit();		
}

//Tout est ok, on affiche le contenu
?>
$('#residenceEditForm').fadeIn();