<?
require("include/config.inc.php");
require("include/fonctions.inc.php");
$db = common_mysql_connect(MYSQL_DB);
require("include/header.inc.php");

ident_page_protect(1);//Gestion clients

?>
<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->

        <ul class="breadcrumb">
            <li><a href="#">Accueil</a></li>

            <li><a href="#">Administration</a></li>

            <li class="active">Liste clients</li>
        </ul><!--breadcrumbs end -->
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
		<?
		//Recherche des programmes
		$result_liste 	= th_clients_search( $db, 0, 9999, '', '' );
		$nb_total	 	= $result_liste['nb_results_total'];
		$nb_liste	 	= $result_liste['nb_results_liste'];
		$datas		 	= $result_liste['results'];
		$i = 0;
		
		if($nb_liste > 0)
		{
		    ?>
            <section class="panel">
                <header class="panel-heading">
                    Liste des clients
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                     </span>
                </header>
                <div class="panel-body">
	                <div class="adv-table">
	              
						<div class="clearfix">
		                    <div class="btn-group">
		                        <button data-toggle="modal" data-target="#clientEditModal" class="btn btn-primary" onClick="clientEdit('0');">
		                            Ajouter un client <i class="fa fa-plus"></i>
		                        </button>
		                    </div>
		                </div>                
	
						<table  class="display table table-bordered table-striped dynamic-table">
						<thead>
						<tr>
						    <th>Nom</th>
						    <th>Client depuis</th>
						    <th>Type</th>
						    <th>Ville</th>
						    <th>Nb résidences</th>
						    <th>Actions</th>
						</tr>
						</thead>
						<tbody>
						    <?  
						    foreach($datas as $id => $null)
						    {
						    	if(is_array($null))
						    	{
						        	$ID_client 	= $datas[$id]['ID_client'];
						        	$clients_datas	= th_clients_get_datas($db,$ID_client);	
						        	?>
						    		<tr class="gradeX" id="client_liste_<? print $ID_client?>">

						    		    <td>
						    				<a href="clients-detail.php?id=<? print $ID_client;?>">								    
						    					<?
						    					print str_truncate(str_txt_format($clients_datas['client_rs']), 200);
						    					?>									
						    	    		</a>
						    		    </td>
						    		    <td>
						    		    	<? print date(CFG_DATE_SHORT_FORMAT, strtotime($clients_datas['client_depuis']));?>
						    		    </td>						    		    
						    		    <td>
						    		    	<? print $MYSQL_ENUM_DATA_CLIENT_TYPE[$clients_datas['client_type']];?>
						    		    </td>							    
						    		    <td>
						    				<a href="#edit" data-toggle="modal" data-target="#clientEditModal" onClick="clientEdit('<? print $ID_client;?>');">								    
						    					<?
						    					print str_truncate(str_txt_format($clients_datas['client_ville']), 60);
						    					?>
						    				</a>								    
						    		    </td>
						    		    <td>
						    				<a href="#edit" data-toggle="modal" data-target="#clientEditModal" onClick="clientEdit('<? print $ID_client;?>');">								    
						    		    		<? 
							    		    	print th_residences_nb_for_client($db, $ID_client);
							    		    	?>
						    				</a>								    
						    		    </td>
						    		    <td class="center">
						    				<a href="clients-detail.php?id=<? print $ID_client;?>" onClick="clientEdit('<? print $ID_client;?>');">								    
						    					fiche
						    				</a>	
						    				|									
						    				<a href="#edit" data-toggle="modal" data-target="#clientEditModal"  onClick="clientEdit('<? print $ID_client;?>');">								    
						    					modifier
						    				</a>	
						    				|									
						    				<a href="#suppr" data-toggle="modal" data-target="#confirmDeleteModal"  onClick="clientDeleteConfirm('<? print $ID_client;?>');">								    
						    					supprimer
						    				</a>	
						    											    
						    		    </td>
						    		</tr>
						        	<?
						        }
						    }
						    ?>
						</tbody>
						<tfoot>
						<tr>
						    <th>Nom</th>
						    <th>Client depuis</th>
						    <th>Type</th>
						    <th>Ville</th>
						    <th>Nb résidences</th>
						    <th>Actions</th>
						</tr>
						</tfoot>
						</table>
	                </div>
                </div>
            </section>
		    <?
		}
		else
		{
		    ?>
		    <p align="center">
		    	Aucun client
				<div class="clearfix">
				    <div class="btn-group">
				        <button data-toggle="modal" data-target="#clientEditModal" class="btn btn-primary" onClick="clientEdit('0');">
				            Ajouter un client <i class="fa fa-plus"></i>
				        </button>
				    </div>
				</div>  		    
		    </p>
		    <?
		}
		?>
	</div>
</div>		

<?
require("include/footer.inc.php");
?>