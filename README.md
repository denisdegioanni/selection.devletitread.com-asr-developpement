# README #

## Structure de la table contact ##

````
CREATE TABLE `th_clients_contacts` (
  `ID_contact` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `ID_client` int(6) unsigned DEFAULT NULL,
  `contact_nom` varchar(100) NOT NULL DEFAULT '',
  `contact_prenom` varchar(100) NOT NULL DEFAULT '',
  `contact_fonction` int(2) unsigned NOT NULL,
  `contact_tel` varchar(20) NOT NULL DEFAULT '',
  `contact_email` varchar(150) NOT NULL DEFAULT '',
  `contact_status` enum('ACTIF','SUPPRIME') NOT NULL DEFAULT 'ACTIF',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_contact`),
  KEY `ID_client` (`ID_client`),
  KEY `contact_status` (`contact_status`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `th_clients_contacts` (`ID_contact`, `ID_client`, `contact_nom`, `contact_prenom`, `contact_fonction`, `contact_tel`, `contact_email`, `contact_status`, `timestamp`)

VALUES
(4,6,'Dupont','Pierre',1,'06 07 08 09 10','pierre@dupont.com','ACTIF','2015-09-15 11:06:42');
````

## Message ##
Suite au dépôt de mon annonce, j’ai reçu énormément de réponses et de propositions de collaboration
Beaucoup de profils sont de très bonne qualité, mais il m’est très difficile de procéder à un choix.

Je souhaiterais proposer à ceux et celles qui sont d’accord de réaliser un petit test de développement qui vous permettra de votre côté de voir si notre façon de travailler vous convient et vice versa

Le test est relativement simple, je vous donne accès au code que nous utilisons pour certains de nos intranets. La version de test, limitée à une seule section est accessible ici :

````
URL : http://selection.devletitread.com
Login : Admin
Pwd : pwd4admin
````
Le test est simple, allez sur la page : Gestions clients/Liste des clients
Sur cette page il est possible d’éditer la fiche client ainsi que la liste des résidences. J’ai volontairement désactivé l’ajout/modification/suppression d’un contact ainsi que la table qui liste les contacts

**Je que je souhaiterais pour bien identifier ceux avec qui nous pourrions collaborer il faudrait juste :**

* créer la fonction « Ajouter un contact »
*  et le tableau listant les contacts

En vous basant sur ce qui a été fait pour l’ajout d’une résidence.

Pour le tableau des contacts, j’aimerais pour ce test les colonnes suivantes :

Nom : prénom + nom du contact
e-Mail : email du contact
Fonction : nom de la fonction du contact
Actions : modifier | supprimer

Encore une fois, je propose ce test uniquement à ceux qui le souhaitent, je suis indépendant comme vous et je sais la valeur du temps. Donc si vous ne voulez pas vous y soumettre il n’y a vraiment aucun souci.

Si toutefois vous êtes intéressé à participer à cette sélection, je vous donnerais accès au une URL GitHub et je vous créerais une branche pour que vous puissiez travailler dessus. Je vous communiquerais également la structure de la table des contacts

###QUELS SONT LES POINTS IMPORTANTS POUR LA SÉLECTION###

* Votre capacité à vous adapter à du code existant, même si celui-ci n’est pas conventionnel
* Votre Réactivité
* Votre capacité à réaliser une interface propre et en correspondance avec l'existant
* Votre capacité à utiliser un outil de versionning

Question subsidiaire : j’aimerais remplacer le picto du menu gestion clients par celui-ci : http://fortawesome.github.io/Font-Awesome/icon/television/

Enfin, pouvez-bous me communiquer l’URL d’un ou deux projet que vous avez développé ?

Merci à tous