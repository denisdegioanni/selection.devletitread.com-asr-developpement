<?
require_once(dirname(__FILE__).'/include/config.inc.php');
require_once(dirname(__FILE__).'/include/fonctions.inc.php');

$db = common_mysql_connect(MYSQL_DB);

ident_page_protect(1);//Gestion residences


//Affichage liste champs pour recup
/*
foreach($_POST as $field_name => $value)
{
	print '$'.$field_name.' = $_POST[\''.$field_name.'\'];<br/>';
}
foreach($_POST as $field_name => $value)
{
	print '$'.$field_name.' = $_POST[\''.$field_name.'\'];'."\n";
}
exit();
*/

$errfield 		= $_POST['errfield'];
$ID_residence 	= $_POST['ID_residence'];
$ID_client	 	= $_POST['ID_client'];
$nom 			= strtoupper(common_remove_accents($_POST['nom']));
$adresse 		= $_POST['adresse'];
$adresse_suite 	= $_POST['adresse_suite'];
$cpost 			= $_POST['cpost'];
$ville 			= strtoupper(common_remove_accents($_POST['ville']));
$pays 			= $_POST['pays'];
$batiment	 	= $_POST['batiment'];
$entree	 		= $_POST['entree'];
$nfcId			= $_POST['nfcId'];

//Verif champs obligatoires
$mandatory_fields = array('ID_residence', 'nom', 'cpost', 'ville', 'pays');

foreach($mandatory_fields as $null => $mand_field_name)
{
	$_POST[$mand_field_name] = trim($_POST[$mand_field_name]);
	if($_POST[$mand_field_name] == '')
	{
		//Champ obligatoire non rempli (teste au niveau JS normalement donc on renvoie une erreur sommaire)
		?>
		$('#<? print $errfield;?>').html("<div class=\"alert alert-danger\">Un champ obligatoire (<? print $mand_field_name;?>) n'a pas été correctement rempli. Merci de vérifier le formulaire.</div>");
		<?
		exit();
	}
}

if($ID_residence == 0 && ($ID_client == 0 || $ID_client == 'undefined' || !is_numeric($ID_client)))
{
	?>
	$('#<? print $errfield;?>').html("<div class=\"alert alert-danger\">Aucun client sélectionné, nous ne pouvons créer une résidence, merci de raffraichir la page et de réessayer. Si le problème persiste, merci le signaler à l'équipe de développement.</div>");
	<?
	exit();
}
elseif($ID_residence == 0)
{
	//Creation nouvel enregistrement
	$ID_residence = th_residences_create($db);
	th_residences_update_field($db, $ID_residence, 'ID_client', $ID_client);	
	$creation = 1;
	
}
else
{
	$creation = 0;
	if($residence_datas = th_residences_get_datas($db, $ID_residence))
	{
	}
	else
	{
		?>
		$('#<? print $errfield;?>').html("<div class=\"alert alert-danger\">Impossible d'editer la fiche <? print $ID_residence;?>...Erreur technique...</div>");
		<?
		exit();		
	}
	
}

//Mise a jour des champs
th_residences_update_field($db, $ID_residence, 'residence_nom', $nom);
th_residences_update_field($db, $ID_residence, 'residence_adresse', $adresse);
th_residences_update_field($db, $ID_residence, 'residence_adresse_suite', $adresse_suite);
th_residences_update_field($db, $ID_residence, 'residence_cpost', $cpost);
th_residences_update_field($db, $ID_residence, 'residence_ville', $ville);
th_residences_update_field($db, $ID_residence, 'residence_pays', $adresse_pays);
th_residences_update_field($db, $ID_residence, 'residence_batiment', $batiment);
th_residences_update_field($db, $ID_residence, 'residence_entree', $entree);
th_residences_update_field($db, $ID_residence, 'residence_nfcId', $nfcId);

//Tout est ok, on ferme la modale
if($creation == 1)
{
?>
	alert("Résidence enregistrée !");
	window.location.reload();
<?
}
else
{
?>
	$('#residenceEditModal').modal('hide');
	window.location.reload();
<?
}
?>