<?
require("include/config.inc.php");
require("include/fonctions.inc.php");

$db = common_mysql_connect(MYSQL_DB);

$login 		= $_REQUEST['login'];
$mdp   		= $_REQUEST['mdp'];
$errfield 	= $_REQUEST['errfield'];

if( $ID_intervenant = intervenant_exists_login($db, $login) )//Login admin
{
	$intervenant_datas = intervenants_get_datas($db, $ID_intervenant);
	
	if($intervenant_datas['intervenant_pwd'] == $mdp)
	{
		$_SESSION['LOGIN']['type']			 = 'admin';
		$_SESSION['LOGIN']['ID_intervenant'] = $ID_intervenant;//Identification OK, utilisateur lambda
		$_SESSION['LOGIN']['ID_admrole'] 	 = $intervenant_datas['ID_admrole'];//Identification OK, utilisateur lambda
		
		$admrole_datas = cfg_admin_roles_get_datas($db, $intervenant_datas['ID_admrole']);
		$_SESSION['LOGIN']['admrights'] 	= explode('|', $admrole_datas['IDs_admrights']);//Droits
		
		//MAJ date dernier login
		intervenants_update_field( $db, $_SESSION['LOGIN']['ID_intervenant'], 'intervenant_lastaccess', date('Y-m-d H:i:s'));
		?>
		window.location = '<? print  $admrole_datas['admrole_defpage'];?>';
		<?
	}
	else
	{
		?>
		$('#<? print $errfield;?>').html('<div class="alert alert-danger">Mot de passe incorrect</div>');
		<?
	}
}
elseif( $ID_client = th_clients_exists_email($db, $login) )//Login client => espace client
{
	$intervenant_datas = th_clients_get_datas($db, $ID_client);
	
	if($intervenant_datas['client_password'] == $mdp)
	{
		$_SESSION['LOGIN']['type']			 = 'client';
		$_SESSION['LOGIN']['ID_intervenant'] = $ID_client;//Identification OK, utilisateur lambda
		$_SESSION['LOGIN']['ID_admrole'] 	 = $intervenant_datas['ID_admrole'];//Identification OK, utilisateur lambda
		
		$admrole_datas = cfg_admin_roles_get_datas($db, CFG_ID_ADMROLE_CLIENT);
		$_SESSION['LOGIN']['admrights'] 	= explode('|', $admrole_datas['IDs_admrights']);//Droits
		
		//MAJ date dernier login
		th_clients_update_field( $db, $ID_client, 'client_lastaccess', date('Y-m-d H:i:s'));
		?>
		window.location = '<? print  $admrole_datas['admrole_defpage'];?>';
		<?
	}
	else
	{
		?>
		$('#<? print $errfield;?>').html('<div class="alert alert-danger">Mot de passe incorrect</div>');
		<?
	}	
}
else
{
	?>
    $('#<? print $errfield;?>').html('<div class="alert alert-danger">Nom d\'utilisateur incorrect</div>');
    <?
}
?>