<?
require_once(dirname(__FILE__).'/include/config.inc.php');
require_once(dirname(__FILE__).'/include/fonctions.inc.php');

$db = common_mysql_connect(MYSQL_DB);

ident_page_protect(1);//Gestion clients

$ID_client = $_REQUEST['id'];

if($ID_client == '0')
{
	?>
	$('#clientEditForm input[name="ID_client"]').val("0");	
	$('#clientEditForm input[type=text],#clientEditForm input[type=date],#clientEditForm input[type=number]').val('');	
	$('#clientEditFormLabel').html('Nouveau client');
	<?
}
elseif($client_datas = th_clients_get_datas($db,$ID_client))//Verification droit
{
   	?>
   	if($('#clientEditForm').length> 0)
   	{
   		$('#clientEditForm input[name="ID_client"]').val("<? print $ID_client;?>");
   		$('#clientEditForm input[name="email"]').val('<? print addslashes($client_datas['client_email']);?>');
   		$('#clientEditForm select[name="type"]').val('<? print addslashes($client_datas['client_type']);?>');
   		$('#clientEditForm input[name="date_depuis"]').val('<? print date(CFG_DATE_SHORT_FORMAT, strtotime($client_datas['client_depuis']));?>');
   		$('#clientEditForm input[name="rs"]').val('<? print addslashes($client_datas['client_rs']);?>');
   		$('#clientEditForm input[name="adresse"]').val('<? print addslashes($client_datas['client_adresse']);?>');
   		$('#clientEditForm input[name="adresse_suite"]').val('<? print addslashes($client_datas['client_adresse_suite']);?>');
   		$('#clientEditForm input[name="cpost"]').val('<? print addslashes($client_datas['client_cpost']);?>');
   		$('#clientEditForm input[name="ville"]').val('<? print addslashes($client_datas['client_ville']);?>');
   		$('#clientEditForm input[name="pays"]').val('<? print addslashes($client_datas['client_pays']);?>');
   		$('#clientEditForm input[name="email_dedie"]').val('<? print addslashes($client_datas['client_email_dedie']);?>');
   		$('#clientEditForm select[name="zone"]').val('<? print addslashes($client_datas['client_zone']);?>');
   		
   		$('#clientEditFormLabel').html('<? print addslashes($client_datas['client_rs']);?>');
   	}
   	<?		
}
else
{
	?>
	alert("Fiche inexistante (ne peut être modifiée).");
	<?
	exit();		
}

//Tout est ok, on affiche le contenu
?>
$('#clientEditForm').fadeIn();