<?
//Generation des fonctions automatiques search, get, ...
require("include/config.inc.php");
require("include/fonctions.inc.php");

$id_table ++; 
$table[$id_table]['name']  = 'th_ampoules_comment';

function get_table_fields($table_name, $db)
{
	if($R1 = common_mysql_query($db, "SHOW FIELDS FROM ".$table_name))
	{
		while( $F1 = mysqli_fetch_row($R1) )
		{
			$ret_datas[] = $F1[0];
		}
		return($ret_datas);
	}
	else
	{
		return(false);
	}
}

//Recup du code de base
$code_base = file_get_contents('func_generate.txt');

$db = common_mysql_connect(MYSQL_DB);

print "\n\n\nFICHIER FONCTIONS\n\n\n";

foreach( $table as $ID_table => $table_name )
{
	$table_name = $table[$ID_table]['name'];
	
	//print 'TABLE NAME '.$table_name.' :<br/>';
	
	if($table_fields = get_table_fields( $table_name, $db ))
	{		
		$other_fields_lst = '';
		$other_fields_sql = '';
		foreach( $table_fields as $num_field => $field_name )
		{
			if($num_field == 0)
			{
				$primary = $field_name;	
			}
			else
			{
				if($other_fields_lst != '')
				{
					$other_fields_lst .= ', ';
					$other_fields_sql .= ', ';
				}
				$other_fields_lst .= '$'.$field_name;
				$other_fields_sql .= $field_name.' = \'".common_mysql_encode($'.$field_name.')."\'';
			}
		}
		
		if($num_field > 0)
		{
			$other_fields_lst = '$'.$primary.','.$other_fields_lst;
			$other_fields_sql_with_primary 	  = ''.$primary.' = \'".$'.$primary.'."\', '.$other_fields_sql;
			$other_fields_sql_without_primary = $other_fields_sql;
		}
		else
		{
			$other_fields_lst = $primary;
			$other_fields_sql_with_primary = $primary.' = \'".$'.$primary.'."\'';
		}
		
		//print 'PRIMARY : '.$primary.'<br/>';
		//print 'OTHER FIELDS LST : '.$other_fields_lst.'<br/>';
		//print 'OTHER FIELDS VAR : '.$other_fields_var.'<br/>';
		
		$liste_dbs_name = 'MYSQL_TABLE_'.strtoupper($table_name);
		
		$conf_code  .= "define('".$liste_dbs_name."', '".$table_name."');\n"; 
		
		$final_code = str_replace('NEWFUNC', $table_name, $code_base);
		$final_code = str_replace('LISTE_ARGS', $other_fields_lst, $final_code);
		$final_code = str_replace('MYSQL_TABLE_NAME', 'MYSQL_TABLE_'.strtoupper($table_name), $final_code);
		$final_code = str_replace('UPDATE_FIELDS_SQL_WITH_PRIMARY', $other_fields_sql_with_primary, $final_code);
		$final_code = str_replace('UPDATE_FIELDS_SQL_WITHOUT_PRIMARY', $other_fields_sql_without_primary, $final_code);
		$final_code = str_replace('PRIMARY_KEY_FIELD', $primary, $final_code);
		
		print '/*Fonctions '.$table_name.' ('.$liste_dbs_name.')*/'."\n\n".$final_code."\n\n".'/*Fin '.$table_name.' ('.$liste_dbs_name.")*/\n\n";
		
	}
}

print "\n\n\nFICHIER CONFIG\n\n\n".$conf_code;

?>