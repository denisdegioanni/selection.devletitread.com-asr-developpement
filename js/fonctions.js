/*************************
* NouveauProjet          *
* Foctions JS 			 *
*************************/

var img_loader = '<img src="/img/ajax-loader.gif" width="16" height="16">';
var debug_console = 1;

function frenchDateToSql(frenchDate)
{
	var newDate = frenchDate.substring(6, 10)+'-'+frenchDate.substring(3, 5)+'-'+frenchDate.substring(0, 2);
	return(newDate);
}
function sqlDateToFrench(frenchDate)
{
	var newDate = frenchDate.substring(8, 10)+'/'+frenchDate.substring(5, 7)+'/'+frenchDate.substring(0, 4);
	return(newDate);
}

//LOGIN
function loginCheck(login, pwd, alertField)
{
	$("#"+alertField).removeClass("error");
	$('#'+alertField).html(img_loader+' Identification en cours...');
	
	var loginVal = $("#"+login).val();
	var pwdVal   = $("#"+pwd).val();
	
	$.ajax({
	  url: './login-check.php',
	  type: 'POST',
	  data: 'login='+loginVal+'&mdp='+pwdVal,
	  dataType: 'json',
	  success: function(data) 
	  {
	  		switch(data.status)
	  		{
				case '-1':
					$("#"+alertField).html("Utilisateur non trouvé.");
					$("#"+alertField).addClass("error");
				break;
				case '-2':
					$("#"+alertField).html("Mot de passe incorrect.");
					$("#"+alertField).addClass("error");
				break;
				default:
					window.location = data.returnpage;
				break;
	  		}
	  }
	});	
}

//var debug_console = 1;
var img_loader = '<img src="/img/ajax-loader.gif" width="16" height="16">';

function formFieldTest(formId, errFieldId)
{
	$('#'+errFieldId).html("");
	
	//Verification des champs
	var errStr = '';	

 	$('#'+formId+' input[mandatory_type], #'+formId+' textarea[mandatory_type], #'+formId+' select[mandatory_type]').each(function (i) {

    		var fieldName 	= $(this).attr('name');
    		var fieldValue 	= $(this).val();
    		var mandType 	= $(this).attr('mandatory_type');	  
    		var mandErr 	= $(this).attr('mandatory_fielderr');	  

			if(debug_console == 1)
			{
				console.log(  "Controle formulaire : " + formId+", champ : "+fieldName+', val : '+fieldValue );				
			}

			$(this).parent().removeClass('has-error');			
			
			switch(mandType)
			{
				case 'string':

					var mandValue 	= $(this).attr('mandatory_value');	
					if(mandValue.indexOf(',') > 0)
					{
						mandValueArr = mandValue.split(',');
						
						if(fieldValue.length < mandValueArr[0] || fieldValue.length > mandValueArr[1])
						{
							errStr += mandErr+"<br/>";			
							$(this).parent().addClass('has-error');			
						} 						
					}
					else
					{
						if(fieldValue.length < mandValue)
						{
							errStr += mandErr+"<br/>";						
							$(this).parent().addClass('has-error');			
						}  						
					}


				break;
				
				case 'number':

					var mandValue 	= $(this).attr('mandatory_value');	
					if(fieldValue.length < mandValue || isNaN(fieldValue))
					{
						errStr += mandErr+"<br/>";	
						$(this).parent().addClass('has-error');														
					}  


				break;				
				
				case 'date':

					if(dateCheck(fieldValue) == 0)
					{
						errStr += mandErr+"<br/>";		
						$(this).parent().addClass('has-error');													
					}  

				break;				
				

				case 'email':
					
					if((fieldValue.indexOf('@') > 0) && (fieldValue.indexOf('.') > 0))
					{
						//OK
					}
					else
					{
						errStr += mandErr+"<br/>";	
						$(this).parent().addClass('has-error');																				
					}

				break;
			}
			
    });
    
	//Affichage erreur ou affichage recap ?
	if(errStr != "")
	{
		$('#'+errFieldId).html('<div class="alert alert-danger">'+errStr+'</div>');
	}
	else
	{
		//OK Submit
		if($('#'+formId).attr('submit-ajax') == 1 )
		{
			formSubmitAjax(formId);
		}
		else
		{
			$('#'+formId).submit();
		}
	}
}

function formSubmitAjax(formId)
{
	//Envoi du formulaire
    $.ajax(
    {
    	  url: $('#'+formId).attr('action'),
    	  type: 'POST',
    	  data: $('#'+formId+' input, #'+formId+' select, #'+formId+' textarea').serialize(),    								  
    	  dataType: 'script',
    	  success: function(data) 
    	  {
    	  	  /*
    		  //Popup pour proposer le reglement
    		  $('#contactOK').modal({
    		      show:true			
    		  });
    		  */
    	  }
    });
}

//Fonctions travaux
/*
function travailEdit(ID_travail)
{
	$('#travailEditForm').hide();
	$('#travailEditFormLabel').html('Chargement...');
    $.ajax(
    {
    	  url: 'travail-edition-load.php',
    	  type: 'POST',
    	  data: '&id='+ID_travail,    								  
    	  dataType: 'script'
    });
}

function travailStatusEdit(ID_travail, defaultValueSelect)
{
	if(typeof(defaultValueSelect) == 'undefined')
	{
		defaultValueSelect = '';
	}
    $.ajax(
    {
    	  url: 'travail-edition-status-load.php',
    	  type: 'POST',
    	  data: '&id='+ID_travail+'&default='+defaultValueSelect,    								  
    	  dataType: 'script'
    });
}

function travailDeleteConfirm(ID_travail)
{
	$('#confirmDeleteBtn').click(function(event) {
		travailDelete(ID_travail);
	});
}

function travailDelete(ID_travail)
{	
    $.ajax(
    {
    	  url: 'travail-delete.php',
    	  type: 'POST',
    	  data: '&id='+ID_travail,    								  
    	  dataType: 'script'
    });
}
*/

//Fonctions intervenants
function intervenantEdit(ID_intervenant)
{
	$('#intervenantEditForm').hide();
	$('#intervenantEditFormLabel').html('Chargement...');
    $.ajax(
    {
    	  url: 'intervenant-edition-load.php',
    	  type: 'POST',
    	  data: '&id='+ID_intervenant,    								  
    	  dataType: 'script'
    });
}

function intervenantDeleteConfirm(ID_intervenant)
{
	$('#confirmDeleteBtn').click(function(event) {
		intervenantDelete(ID_intervenant);
	});
}

function intervenantDelete(ID_intervenant)
{	
    $.ajax(
    {
    	  url: 'intervenant-delete.php',
    	  type: 'POST',
    	  data: '&id='+ID_intervenant,    								  
    	  dataType: 'script'
    });
}

//Fonctions techniciens
function technicienEdit(ID_technicien)
{
	$('#technicienEditForm').hide();
	$('#technicienEditFormLabel').html('Chargement...');
    $.ajax(
    {
    	  url: 'technicien-edition-load.php',
    	  type: 'POST',
    	  data: '&id='+ID_technicien,    								  
    	  dataType: 'script'
    });
}

function technicienDeleteConfirm(ID_technicien)
{
	$('#confirmDeleteBtn').click(function(event) {
		technicienDelete(ID_technicien);
	});
}

function technicienDelete(ID_technicien)
{	
    $.ajax(
    {
    	  url: 'technicien-delete.php',
    	  type: 'POST',
    	  data: '&id='+ID_technicien,    								  
    	  dataType: 'script'
    });
}

//Fonction espace client profil modales
function espclientProfilEdit()
{
	$('#espclientProfilForm').hide();
	$('#espclientProfilFormLabel').html('Chargement...');
    $.ajax(
    {
    	  url: 'espclient-profil-edition-load.php',
    	  type: 'POST',
    	  data: '',    								  
    	  dataType: 'script'
    });
}

//Fonctions clients modales
function clientLogoUpdate(filename, ID_client)
{
	$('#clientEditForm').hide();
	$('#clientEditFormLabel').html('Chargement...');
    $.ajax(
    {
    	  url: 'client-logo-process.php',
    	  type: 'POST',
    	  data: '&id='+ID_client+'&filename='+encodeURIComponent(filename),    								  
    	  dataType: 'script'
    });	
}

function clientLogoDel(ID_client)
{
	if(confirm("Etes vous sur de vouloir supprimer le logo du client ?"))
	{
		window.location = 'clients-detail.php?id='+ID_client+'&logodel=1';
	}
}

function clientEdit(ID_client)
{
	$('#clientEditForm').hide();
	$('#clientEditFormLabel').html('Chargement...');
    $.ajax(
    {
    	  url: 'client-edition-load.php',
    	  type: 'POST',
    	  data: '&id='+ID_client,    								  
    	  dataType: 'script'
    });
}

function clientDeleteConfirm(ID_client)
{
	$('#confirmDeleteBtn').click(function(event) {
		clientDelete(ID_client);
	});
}

function clientDelete(ID_client)
{	
    $.ajax(
    {
    	  url: 'client-delete.php',
    	  type: 'POST',
    	  data: '&id='+ID_client,    								  
    	  dataType: 'script'
    });
}

//Fonctions pointage alertes
function pointageAlerteDeleteConfirm(ID_passalerte)
{
	$('#confirmDeleteBtn').click(function(event) {
		pointageAlerteDelete(ID_passalerte);
	});
}

function pointageAlerteDelete(ID_passalerte)
{	
    $.ajax(
    {
    	  url: 'pointage-alerte-delete.php',
    	  type: 'POST',
    	  data: '&id='+ID_passalerte,    								  
    	  dataType: 'script'
    });
}

//Fonctions pointage passages
function pointagePassageEdit(ID_passage)
{
	$('#pointagePassageEditForm').hide();
	$('#pointagePassageEditFormLabel').html('Chargement...');
    $.ajax(
    {
    	  url: 'pointage-passage-edition-load.php',
    	  type: 'POST',
    	  data: '&id='+ID_passage,    								  
    	  dataType: 'script'
    });
}

function pointagePassageDeleteConfirm(ID_passage)
{
	$('#confirmDeleteBtn').click(function(event) {
		pointagePassageDelete(ID_passage);
	});
}

function pointagePassageDelete(ID_passage)
{	
    $.ajax(
    {
    	  url: 'pointage-passage-delete.php',
    	  type: 'POST',
    	  data: '&id='+ID_passage,    								  
    	  dataType: 'script'
    });
}

function pointagePassageEditOnLoad()//A initialiser apres chargement
{
	$('#pointagePassageEditModal select[name=client]').on('change', function() {
		pointagePassageEditActionOnChange( $(this).val(), '' );
	});
	
}

function pointagePassageEditActionOnChange(clientVal, residenceSelected)
{
	if(clientVal == '')
	{
		$('#pointagePassageEditModalListeResidencesClient').hide();
		$('#pointagePassageEditModal .modal-footer').hide();
		
		$('#pointagePassageEditModalAttenteClient').fadeIn();		
	}
	else
	{
		$('#pointagePassageEditModalListeResidencesClient').show();
		$('#pointagePassageEditModalAttenteClient').hide();		
		$('#pointagePassageEditModal .modal-footer').show();

		//Changement du select residence => construction liste residences
		pointagePassageEditSelectChange(clientVal, residenceSelected);			
	}	
}

function pointagePassageEditSelectChange(ID_client, selected_ID_residence)
{
	$.ajax(
	{
		  url: 'pointage-passage-edition-select-client.php',
		  type: 'POST',
		  data: '&client='+ID_client+'&residence='+selected_ID_residence,    								  
		  dataType: 'script'
	});	
}




//Fonctions pointage planing modales
function pointagePlanningEdit(ID_passplaning)
{
	$('#pointagePlanningEditForm').hide();
	$('#pointagePlanningEditFormLabel').html('Chargement...');
    $.ajax(
    {
    	  url: 'pointage-planing-edition-load.php',
    	  type: 'POST',
    	  data: '&id='+ID_passplaning,    								  
    	  dataType: 'script'
    });
}

function pointagePlanningDeleteConfirm(ID_passplaning)
{
	$('#confirmDeleteBtn').click(function(event) {
		pointagePlanningDelete(ID_passplaning);
	});
}

function pointagePlanningDelete(ID_passplaning)
{	
    $.ajax(
    {
    	  url: 'pointage-planing-delete.php',
    	  type: 'POST',
    	  data: '&id='+ID_passplaning,    								  
    	  dataType: 'script'
    });
}

function pointagePlanningEditOnLoad()//A initialiser apres chargement
{
	$('#pointagePlanningEditForm select[name=periodicite_type]').on('change', function() {

		switch($(this).val())
		{
			case '3'://Periodicite jusqu'au XX
			
				$('.showIfPeriodicite3').fadeIn();
				$('.hideIfPeriodicite1').fadeIn();
			
			break;
			case '2'://Periodicite sans date limite
			
				$('.showIfPeriodicite3').fadeOut();
				$('.hideIfPeriodicite1').fadeIn();
			
			break;
			case '1'://Ponctuelle
				$('.showIfPeriodicite3').fadeOut();
				$('.hideIfPeriodicite1').fadeOut();
			
			break;			
		}
	});	
	
	//Traitement des valeurs dates exclusions quand ajout dans input hidden
	$('#pointagePlanningEditForm input[name=liste_date_exclusions]').on('change', function() {
		
		var currentDateExcluesListe = $(this).val();
		if( $(this).val() == '' )
		{
			$('#pointagePlaningDateExcluesLst').html('Aucune date exclue pour cette planification');
		}
		else
		{
			//Traitement des dates et affichage liste
			$('#pointagePlaningDateExcluesLst').html('');
			var arr = currentDateExcluesListe.split(',');
			
			$.each( arr, function( key, value ) {
				if(value != '')
				{
					$('#pointagePlaningDateExcluesLst').append('<div style="margin-bottom:3px;"><i class="fa fa-check-circle"></i> '+sqlDateToFrench(value)+'&nbsp;&nbsp;<a href="#del" deldateexclude="'+sqlDateToFrench(value)+'" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i>Suppr</a></div>');					
				}
			});
		}
		
	});
	
	//Bouton d'ajout de valeur dans date exclusion	
	$('#pointagePlanningEditAddExclusion').on('click', function() {
		var curDateExclusionAdd = $('#pointagePlanningEditForm input[name=date_exclusion_add]').val();
		if(curDateExclusionAdd.length == 10 && curDateExclusionAdd.indexOf('/') > 0)
		{
			var currentDateExcluesListe = $('#pointagePlanningEditForm input[name=liste_date_exclusions]').val();
			$('#pointagePlanningEditForm input[name=liste_date_exclusions]').val(currentDateExcluesListe+','+frenchDateToSql(curDateExclusionAdd));
			$('#pointagePlanningEditForm input[name=liste_date_exclusions]').trigger('change');
			
			$('#pointagePlanningEditModalErrField').html("<strong>Pensez à enregistrer vos modifications en cliquant sur le bouton Enregistrer avant de fermer cette fenêtre.</strong>");
		}
		else
		{
			alert("Date incorrecte !");
		}
		
	});
	
	//LiveMode pour boutons suppr de date exclusion
	$('#pointagePlaningDateExcluesLst').on('click', 'a[deldateexclude]', function() {
		
		var dateToExclude = $(this).attr('deldateexclude');
		if(confirm("Etes vous sur de vouloir supprimer la date d'exclusion "+dateToExclude+" ? Il faudra ensuite enregistrer la planification pour que la suppression soit prise en compte."))
		{
			//Modification liste resultats dans input hidden (format SQL)
			
			var currentDateExcluesListe = $('#pointagePlanningEditForm input[name=liste_date_exclusions]').val();
			var newDateExcluesListe = currentDateExcluesListe.replace(","+frenchDateToSql(dateToExclude), "");
			$('#pointagePlanningEditForm input[name=liste_date_exclusions]').val(newDateExcluesListe);
			
			$('#pointagePlanningEditForm input[name=liste_date_exclusions]').trigger('change');
			
			$('#pointagePlanningEditModalErrField').html("<strong>Pensez à enregistrer vos modifications en cliquant sur le bouton Enregistrer avant de fermer cette fenêtre.</strong>");
			
			
		}

	});
	
}
/*
function frenchDateToSql(frenchDate)
{
	var newDate = frenchDate.substring(6, 4)+'-'+frenchDate.substring(2, 2)+'-'+frenchDate.substring(0, 2);
	return(newDate);
}
function sqlDateToFrench(frenchDate)
{
	var newDate = frenchDate.substring(8, 2)+'/'+frenchDate.substring(5, 2)+'/'+frenchDate.substring(0, 4);
	return(newDate);
}	
	
*/
//Fonctions residences modales
function residenceEdit(ID_residence, ID_client)
{
	if(typeof ID_client == 'undefined')
	{
		ID_client = '';
	}

	$('#residenceEditForm').hide();
	$('#residenceEditFormLabel').html('Chargement...');
    $.ajax(
    {
    	  url: 'residence-edition-load.php',
    	  type: 'POST',
    	  data: '&id='+ID_residence+'&client='+ID_client,    								  
    	  dataType: 'script'
    });
}

function residenceDeleteConfirm(ID_residence)
{
	$('#confirmDeleteBtn').click(function(event) {
		residenceDelete(ID_residence);
	});
}

function residenceDelete(ID_residence)
{	
    $.ajax(
    {
    	  url: 'residence-delete.php',
    	  type: 'POST',
    	  data: '&id='+ID_residence,    								  
    	  dataType: 'script'
    });
}


function contactDeleteConfirm(ID_contact)
{
	$('#confirmDeleteBtn').click(function(event) {
		contactDelete(ID_contact);
	});
}

function contactDelete(ID_contact)
{	
    $.ajax(
    {
    	  url: 'contact-delete.php',
    	  type: 'POST',
    	  data: '&id='+ID_contact,    								  
    	  dataType: 'script'
    });
}



//Fonctions ampoules modales
function ampouleEdit(ID_ampoule, ID_residence)
{
	if(typeof ID_client == 'undefined')
	{
		ID_client = '';
	}
	$('#ampouleEditForm').hide();
	$('#ampouleEditFormLabel').html('Chargement...');
    $.ajax(
    {
    	  url: 'ampoule-edition-load.php',
    	  type: 'POST',
    	  data: '&id='+ID_ampoule+'&residence='+ID_residence,    								  
    	  dataType: 'script'
    });
}

function ampouleEditCommentListe(divIDCible, ID_ampoule)
{	
	$('#'+divIDCible).html("Chargement commentaires en cours...");
	var no_cache = new Date;
	$('#'+divIDCible).load('ampoule-edition-load-commentaires-liste.php?id='+ID_ampoule+'&nocache='+encodeURIComponent(no_cache));
}

function ampouleEditInterventionsListe(divIDCible, ID_ampoule)
{	
	$('#'+divIDCible).html("Chargement commentaires en cours...");
	var no_cache = new Date;
	$('#'+divIDCible).load('ampoule-edition-load-interventions-liste.php?id='+ID_ampoule+'&nocache='+encodeURIComponent(no_cache));
}

function ampouleDeleteConfirm(ID_ampoule)
{
	$('#confirmDeleteBtn').click(function(event) {
		ampouleDelete(ID_ampoule);
	});
}

function ampouleDelete(ID_ampoule)
{	
    $.ajax(
    {
    	  url: 'ampoule-delete.php',
    	  type: 'POST',
    	  data: '&id='+ID_ampoule,    								  
    	  dataType: 'script'
    });
}

//Fonctions interventions modales
function interventionEdit(intervention_type, ID_intervention)
{
	if(typeof ID_client == 'undefined')
	{
		ID_client = '';
	}
	$('#interventionEditForm').hide();
	$('#interventionEditFormLabel').html('Chargement...');
    $.ajax(
    {
    	  url: 'intervention-edition-load.php',
    	  type: 'POST',
    	  data: '&id='+ID_intervention+'&type='+intervention_type,    								  
    	  dataType: 'script'
    });
}

function interventionDetail(ID_intervention)
{
	if(typeof ID_client == 'undefined')
	{
		ID_client = '';
	}
	$('#interventionFicheForm').hide();
	$('#interventionFicheFormLabel').html('Chargement...');
    $.ajax(
    {
    	  url: 'intervention-detail-load.php',
    	  type: 'POST',
    	  data: '&id='+ID_intervention,    								  
    	  dataType: 'script'
    });
}

function interventionDeleteConfirm(ID_intervention)
{
	$('#confirmDeleteBtn').click(function(event) {
		interventionDelete(ID_intervention);
	});
}

function interventionDelete(ID_intervention)
{	
    $.ajax(
    {
    	  url: 'intervention-delete.php',
    	  type: 'POST',
    	  data: '&id='+ID_intervention,    								  
    	  dataType: 'script'
    });
}

function interventionEditOnLoad()//A initialiser apres chargement
{
	$('#interventionEditModal select[name=residence]').on('change', function() {
		if($(this).val() == '0')
		{
			$('#interventionEditionChangement').hide();
			$('#interventionEditionDemande').hide();
			$('#interventionEditModal .modal-footer').hide();
			
			$('#interventionEditionAttenteResidence').fadeIn();		
		}
		else
		{
			//Changement du select residence => construction liste contacts + ampoules
			interventionSelectChange($(this).val(), 0, 0);			
		}
	});
	
	$('#interventionEditModal select[name=type]').on('change', function() {
		
		if($('#interventionEditModal select[name=residence]').val() == 0)
		{
			$('#interventionEditionChangement').hide();
			$('#interventionEditionDemande').hide();
			
			$('#interventionEditModal .modal-footer').hide();//Masquage boutons			
			$('#interventionEditionAttenteResidence').fadeIn();//Affichage attente residence
		}
		else if($(this).val() == '1')//Demande intervention
		{
			$('#interventionEditionAttenteResidence').hide();	//Masquage attente residence
			$('#interventionEditModal .modal-footer').show();	//Affichage boutons		
			$('#interventionEditionChangement').hide();
			$('#interventionEditionDemande').fadeIn();
			
		}
		else//Autre types
		{
			$('#interventionEditionAttenteResidence').hide();	//Masquage attente residence
			$('#interventionEditModal .modal-footer').show();	//Affichage boutons		
			$('#interventionEditionDemande').hide();			
			$('#interventionEditionChangement').fadeIn();
		}
		
	});
	
}

function interventionSelectChange(ID_residence, selected_ID_contact, selected_ID_ampoule)
{
	$.ajax(
	{
		  url: 'intervention-edition-residence-select-change.php',
		  type: 'POST',
		  data: '&residence='+ID_residence+'&contact='+selected_ID_contact+'&ampoule='+selected_ID_ampoule,    								  
		  dataType: 'script'
	});	
}


//Fonctions travail ressources
/*
function travailRessourceEdit(ID_ressource, ID_travail, ID_intervenant, dateDefaut, type_ressource)
{
	if(typeof(ID_intervenant) == 'undefined')
	{
		ID_intervenant = '';
	}
	if(typeof(type_ressource) == 'undefined')
	{
		type_ressource = '';
	}	
	if(typeof(dateDefaut) == 'undefined')
	{
		dateDefaut = '';
	}
	$('#travailRessourceEditForm').hide();
	$('#travailRessourceEditFormLabel').html('Chargement...');
    $.ajax(
    {
    	  url: 'travail-ressource-edition-load.php',
    	  type: 'POST',
    	  data: '&id='+ID_ressource+'&travail='+ID_travail+'&intervenant='+ID_intervenant+'&date='+dateDefaut+'&tableau='+type_ressource,    								  
    	  dataType: 'script'
    });
}

function travailRessourceIndispoEdit(ID_ressource, ID_travail, ID_intervenant, dateDefaut, type_ressource)
{
	if(typeof(ID_intervenant) == 'undefined')
	{
		ID_intervenant = '';
	}
	if(typeof(type_ressource) == 'undefined')
	{
		type_ressource = '';
	}	
	if(typeof(dateDefaut) == 'undefined')
	{
		dateDefaut = '';
	}
	$('#travailRessourceEditForm').hide();
	$('#travailRessourceEditFormLabel').html('Chargement...');
    $.ajax(
    {
    	  url: 'travail-ressource-indispo-load.php',
    	  type: 'POST',
    	  data: '&id='+ID_ressource+'&travail='+ID_travail+'&intervenant='+ID_intervenant+'&date='+dateDefaut+'&tableau='+type_ressource,    								  
    	  dataType: 'script'
    });
}

function travailRessourceNoteEdit(ID_ressource, ID_travail, ID_intervenant, dateDefaut, type_ressource)
{
	if(typeof(ID_intervenant) == 'undefined')
	{
		ID_intervenant = '';
	}
	if(typeof(type_ressource) == 'undefined')
	{
		type_ressource = '';
	}	
	if(typeof(dateDefaut) == 'undefined')
	{
		dateDefaut = '';
	}
	$('#travailRessourceEditForm').hide();
	$('#travailRessourceEditFormLabel').html('Chargement...');
    $.ajax(
    {
    	  url: 'travail-ressource-note-load.php',
    	  type: 'POST',
    	  data: '&id='+ID_ressource+'&travail='+ID_travail+'&intervenant='+ID_intervenant+'&date='+dateDefaut+'&tableau='+type_ressource,    								  
    	  dataType: 'script'
    });
}


function travailRessourceDeleteConfirm(ID_ressource)
{
	$('#confirmDeleteBtn').click(function(event) {
		travailRessourceDelete(ID_ressource);
	});
}

function travailRessourceDelete(ID_ressource)
{	
    $.ajax(
    {
    	  url: 'travail-ressource-delete.php',
    	  type: 'POST',
    	  data: '&id='+ID_ressource,    								  
    	  dataType: 'script'
    });
}

function travailRessourceSelectUpdate(type_tableau, ID_intervenant, formId)
{
	$('#'+formId+' select[name=ID_intervenant]').hide();
    $.ajax(
    {
    	  url: 'travail-ressource-build-select-ressource.php',
    	  type: 'POST',
    	  data: '&t='+type_tableau+'&selected='+ID_intervenant+'&form='+formId,    								  
    	  dataType: 'script'
    });
}

function intervenantEditSelectUpdate(ID_admrole, formID)
{
	if(ID_admrole == 11)//L'assistante commerciale doit pouvoir choisir les commerciaux associes
	{
		$('#selectProfilCommerciaux').show('blind');
	}
	else
	{
		$('#selectProfilCommerciaux').hide('blind');		
	}
	
}

function ressourceTableauLoad()
{
	if($('#ressourceTableauConteneur').length > 0)
	{
		var t = $('#ressourceTableauConteneur').attr('t');
		var d = $('#ressourceTableauConteneur').attr('d');
		var p = $('#ressourceTableauConteneur').attr('p');
		var nocache = Math.round(new Date().getTime() / 1000);
		
		$('#ressourceTableauConteneur').load('ressources-tableau-ajax.php?t='+t+'&p='+p+'&d='+d+'&nocache='+nocache);
	}
}

function pjImportFile(filename, ID_travail)
{
    $.ajax(
    {
    	  url: 'travail-pj-add-process.php',
    	  type: 'POST',
    	  data: '&ID_travail='+ID_travail+'&fname='+encodeURIComponent(filename),
    	  dataType: 'script',
    	  success: function(data) 
    	  {
    	  }
    });	
}

function pjLst(ID_travail)
{
	$('#travauxPjLstConteneur').hide();
    $.ajax(
    {
    	  url: 'travail-pj-liste.php',
    	  type: 'POST',
    	  data: '&ID_travail='+ID_travail,
    	  dataType: 'script',
    	  success: function(data) 
    	  {
    	  }
    });	
}

function pjDel(ID_tpj)
{
	$('#travauxPjLstConteneur').hide();
    $.ajax(
    {
    	  url: 'travail-pj-del.php',
    	  type: 'POST',
    	  data: '&ID_tpj='+ID_tpj,
    	  dataType: 'script',
    	  success: function(data) 
    	  {
    	  }
    });	
}
*/
var globalCurIDTravailRessourceModale = 0;
var globalCurIDRessourceRessourceModale = 0;
$(function() {

	var mouseX;
	var mouseY;
	$(document).mousemove( function(e) {
	   mouseX = e.pageX; 
	   mouseY = e.pageY;
	});
  
	//Validation formulaires : verification contenu
	$( "form" ).each(function( index ) {

		$( this ).submit(function( event ) {
			  //alert( "Handler for .submit() called." );
			  event.preventDefault();			  
			  formFieldTest( $(this).attr('id'), $(this).attr('submit-checkfields-error'));
		});
		
	
	});	 
	
    $('.dpYears').datepicker({ weekStart:1, viewMode:'days' });	
    

	//
	
	/*
	$( "select[update-status-travail]" ).each(function( index ) {

		$( this ).change(function( event ) {
			  //alert( "Handler for .submit() called." );
			  var ID_travail  	= $(this).attr('update-status-travail');
			  var statusVal 	= $(this).val();
			  $('span[update-status-travail-txt="'+ID_travail+'"]').fadeIn('slow');
			  $('span[update-status-travail-txt="'+ID_travail+'"]').html('...');
			  $.ajax(
			  {
			  	  url: 'travail-edition-status-update.php',
			  	  type: 'POST',
			  	  data: '&ID_travail='+ID_travail+'&statut='+statusVal,    								  
			  	  dataType: 'script'
			  });			  
		});
	
	});		
	
	//Suppr
	$( "a[delete-travail]" ).each(function( index ) {

		$( this ).click(function( event ) {
			if(confirm("Etes vous surs de vouloir supprimer ce projet ?"))
			{
			  //alert( "Handler for .submit() called." );
			  var ID_travail  	= $(this).attr('delete-travail');
			  var statusVal 	= 'SUPPRIME';
			  $('#travail_liste_'+ID_travail+'').remove();
			  $.ajax(
			  {
			  	  url: 'travail-edition-status-update.php',
			  	  type: 'POST',
			  	  data: '&ID_travail='+ID_travail+'&statut='+statusVal,    								  
			  	  dataType: 'script'
			  });			  				
			}
		});
	
	});
	*/		
  
});



function dateCheck(d) {
      // Cette fonction vérifie le format JJ/MM/AAAA saisi et la validité de la date.
      // Le séparateur est défini dans la variable separateur
      var amin=2014; // année mini
      var amax=2099; // année maxi
      var separateur="/"; // separateur entre jour/mois/annee
      var j=(d.substring(0,2));
      var m=(d.substring(3,5));
      var a=(d.substring(6));
      var ok=1;
      if ( ((isNaN(j))||(j<1)||(j>31)) && (ok==1) ) {
         //alert("Le jour n'est pas correct."); 
         ok=0;
      }
      if ( ((isNaN(m))||(m<1)||(m>12)) && (ok==1) ) {
         //alert("Le mois n'est pas correct."); 
         ok=0;
      }
      if ( ((isNaN(a))||(a<amin)||(a>amax)) && (ok==1) ) {
         //alert("L'année n'est pas correcte."); 
         ok=0;
      }
      if ( ((d.substring(2,3)!=separateur)||(d.substring(5,6)!=separateur)) && (ok==1) ) {
         //alert("Les séparateurs doivent être des "+separateur); 
         ok=0;
      }
      if (ok==1) {
         var d2=new Date(a,m-1,j);
         j2=d2.getDate();
         m2=d2.getMonth()+1;
         a2=d2.getFullYear();
         if (a2<=100) {a2=1900+a2}
         if ( (j!=j2)||(m!=m2)||(a!=a2) ) {
            alert("La date "+d+" n'existe pas !");
            ok=0;
         }
      }
      return ok;
}


function number_format (number, decimals, dec_point, thousands_sep) {
  // From: http://phpjs.org/functions
  // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +     bugfix by: Michael White (http://getsprink.com)
  // +     bugfix by: Benjamin Lupton
  // +     bugfix by: Allan Jensen (http://www.winternet.no)
  // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // +     bugfix by: Howard Yeend
  // +    revised by: Luke Smith (http://lucassmith.name)
  // +     bugfix by: Diogo Resende
  // +     bugfix by: Rival
  // +      input by: Kheang Hok Chin (http://www.distantia.ca/)
  // +   improved by: davook
  // +   improved by: Brett Zamir (http://brett-zamir.me)
  // +      input by: Jay Klehr
  // +   improved by: Brett Zamir (http://brett-zamir.me)
  // +      input by: Amir Habibi (http://www.residence-mixte.com/)
  // +     bugfix by: Brett Zamir (http://brett-zamir.me)
  // +   improved by: Theriault
  // +      input by: Amirouche
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // *     example 1: number_format(1234.56);
  // *     returns 1: '1,235'
  // *     example 2: number_format(1234.56, 2, ',', ' ');
  // *     returns 2: '1 234,56'
  // *     example 3: number_format(1234.5678, 2, '.', '');
  // *     returns 3: '1234.57'
  // *     example 4: number_format(67, 2, ',', '.');
  // *     returns 4: '67,00'
  // *     example 5: number_format(1000);
  // *     returns 5: '1,000'
  // *     example 6: number_format(67.311, 2);
  // *     returns 6: '67.31'
  // *     example 7: number_format(1000.55, 1);
  // *     returns 7: '1,000.6'
  // *     example 8: number_format(67000, 5, ',', '.');
  // *     returns 8: '67.000,00000'
  // *     example 9: number_format(0.9, 0);
  // *     returns 9: '1'
  // *    example 10: number_format('1.20', 2);
  // *    returns 10: '1.20'
  // *    example 11: number_format('1.20', 4);
  // *    returns 11: '1.2000'
  // *    example 12: number_format('1.2000', 3);
  // *    returns 12: '1.200'
  // *    example 13: number_format('1 000,50', 2, '.', ' ');
  // *    returns 13: '100 050.00'
  // Strip all characters but numerical ones.
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function (n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

/*
//Exemple AJAX jQuery JSON

function login_check(login, pwd)
{
	$('#loader').html(img_loader);
	$.ajax({
	  url: 'login-check.php',
	  type: 'POST',
	  data: 'login='+login+'&pwd='+pwd,
	  dataType: 'json',
	  success: function(data) 
	  {
	  		switch(data.status)
	  		{
				TODO
	  		}
	  }
	});
}

// Exemple fenetre modale jQueryUI

function login_lost_pwd()
{
	var titleWin = 'Rappel de mot de passe';
	
	$( "#modalWin" ).dialog(
	{
				title: titleWin, 
				autoOpen: true,
				//height: 300,
				width: 300,
				modal: true,
				position: Array('center', 90),
				open: function() 
				{
		        		//display correct dialog content
			        	$("#modalWin").load("/login-lost-pwd.php");
				},			
				buttons: 
				{
						"Rappel mot de passe": function() 
						{														
								//Affichage des erreurs						
    							$.ajax(
    							{
    								  url: '/login-lost-pwd-process.php',
									  type: 'POST',
									  data: 'mail='+$( "#mailLostPwd" ).val(),    								  
    								  dataType: 'json',
    								  success: function(data) 
    								  {
											switch(data.result)
											{
												TODO
											}
    								  }
    							});
						},
						"Fermer": function() 
						{
							$( this ).dialog( "close" );
						}
				},
				close: function() 
				{
					$( "#mailLostPwd" ).removeClass( "ui-state-error" );
				}
	});
}

// Exemple zone alerte jQueryUI
function alerteZoneShow(titre, texte, autoHide)
{
	//MAJ des datas
	$( "#headerAlerteTitre" ).html(titre);
	$( "#headerAlerteTxt" ).html(texte);
	
	var options = {};
	if(autoHide == 1)
	{
		$( "#headerAlerteClose" ).hide();
		setTimeout(function() {
			$( "#headerAlerteZone" ).show( 'blind', options, 500, alerteZoneAutoHide );
		}, 1000 );				
	}
	else
	{
		$( "#headerAlerteClose" ).show();
		setTimeout(function() {
			$( "#headerAlerteZone" ).show( 'blind', options, 500 );
		}, 1000 );		
	}
}

function alerteZoneHide()
{
	var options = {};
	$( "#headerAlerteZone" ).hide( 'blind', options, 500 );
}

function alerteZoneAutoHide()
{
	setTimeout(function() {
		alerteZoneHide();
	}, 5000 );
}

*/