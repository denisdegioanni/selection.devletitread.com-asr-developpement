<?
require("include/config.inc.php");
require("include/fonctions.inc.php");
$db = common_mysql_connect(MYSQL_DB);
require("include/header.inc.php");

ident_page_protect(1);//Gestion clients

?>
<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->

        <ul class="breadcrumb">
            <li><a href="#">Accueil</a></li>

            <li><a href="#">Administration</a></li>

            <li class="active">Liste clients</li>
        </ul><!--breadcrumbs end -->
    </div>
</div>

<div class="row">
    <div class="col-sm-12">

            <section class="panel">
                <header class="panel-heading">
                    Liste des clients
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                     </span>
                </header>
                <div class="panel-body">
	                <div class="adv-table">
	              
						<div class="clearfix">
		                    <div class="btn-group">
		                        <button data-toggle="modal" data-target="#clientEditModal" class="btn btn-primary" onClick="clientEdit('0');">
		                            Ajouter un client <i class="fa fa-plus"></i>
		                        </button>
		                    </div>
		                </div>                
	
						<table  class="display table table-bordered table-striped" id="dynamic-table-ajax" ajax_script="/clients-liste-ajax.php">
						<thead>
						<tr>
						    <th>Nom</th>
						    <th>Client depuis</th>
						    <th>Type</th>
						    <th>Ville</th>
						    <th>Nb résidences</th>
						    <th>Actions</th>
						</tr>
						</thead>
						<tfoot>
						<tr>
						    <th>Nom</th>
						    <th>Client depuis</th>
						    <th>Type</th>
						    <th>Ville</th>
						    <th>Nb résidences</th>
						    <th>Actions</th>
						</tr>
						</tfoot>
						</table>
	                </div>
                </div>
            </section>

	</div>
</div>		

<?
require("include/footer.inc.php");
?>